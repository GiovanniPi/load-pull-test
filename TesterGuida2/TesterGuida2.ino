/*
Programma per la gestione della guida mobile che permette il posizionamento del carrello in una determinata posizione fornita dall'utente.
Alcuni dati importanti:
num. giri medi: 69881 +- 3
mm per giro: 0.002037
escursione massima: 142.38 +- 0.01
scarto inziale: 6.23
massima escursione a 698MHz: 107.45mm

*/

int p_dir = 4; //pin 2 usao controllo direzione motore: LOW->indietro, HIGH->avanti
int p_vel = 3; //setta velocità rotazione motore con PWM, 0 fermo, 255 max velocità
int fine_motore = 12; //pin fine corsa lato motore
int fine_aperta = 8; //pin fine corsa lato aperto
volatile long int encoder = 0; //numero di giri
char input;
boolean moving = false; //booleana che indica se la guida si sta muovendo o meno, va a false quando raggiunge un finecorsa o viene stoppata
char pin_fine_corsa;
long int target_pos;


void setup() {  //funzione di inizializzazione
  Serial.begin(9600);
  pinMode(p_vel, OUTPUT);
  pinMode(p_dir, OUTPUT);
  pinMode(fine_motore, INPUT_PULLUP);  //controllo pin fine guida lato motore
  pinMode(fine_aperta, INPUT_PULLUP);  //controllo fine guida lato aperto
  analogWrite(p_vel, LOW);
}

void loop(){ //ciclo principale in loop
  if(Serial.available()>0){
    input = Serial.read();
    switch(input){
      case 'p': Serial.print('p');
                target_pos = leggi_seriale();
                Serial.print(target_pos);
                target_pos = target_pos - 34;
                attachInterrupt(0, posiziona_avanti, RISING);
                avanzamento();
                break;
      case 'r': Serial.print('r');
                resetta();
                break;
      case 'e': Serial.print(encoder);
                break;
    }
  }
  //controllo se fine guida (finita indica se in movimento)
  if(moving){     //<-------------------------1
    if(!fine_guida(pin_fine_corsa)){
      moving = false;
      Serial.println(encoder);
      detachInterrupt(0); //toglie l'interrupt
    }
  }
}

void avanzamento(){
  if(!fine_guida(fine_aperta))
    return;
  digitalWrite(p_dir, HIGH);
  analogWrite(p_vel, 255); 
  pin_fine_corsa = fine_aperta;
  moving = true;
}

void resetta(){
  if(!fine_guida(fine_motore))
    Serial.println("In posizione");
  detachInterrupt(0);
  digitalWrite(p_dir, LOW);
  analogWrite(p_vel, 255);
  do{
    if(Serial.available()>0)     //serva a svuotare la serail, in modo che i caratteri passati durante l'azzeramento vengano tutti eliminati
      input = Serial.read();
  }while(fine_guida(fine_motore));
  encoder = 0;
}    

//funzione controllo fine guida
int fine_guida(int pin){
  if(digitalRead(pin)==HIGH){ //controlla se pin high-->guida fine corsa 
      analogWrite(p_vel, 0);
      return 0;
  }
  return 1;
}

//funzione interrupt per encoder direzione avanti
void posiziona_avanti(){
  encoder++;
  if (encoder >= target_pos){
    analogWrite(p_vel, 0);
    moving = false;
  }
}

//routine lettura intero da porta seriale
long int leggi_seriale(){
  long int incomingByte;   
  char Data[8];
  long int i=0;
  unsigned long Tempo;
  do {
      if (Serial.available()) {       
      Data[i] = Serial.read();
      i++;
      Serial.print(Data[i]);
     }     
     if(i<1)Tempo = millis(); 
   } while (i<5&&(millis()-Tempo) < 1000);
  
   Data[i] = 0; 
   incomingByte = atol(Data);  
   i=0;
   
   return incomingByte;
}
