# -*- coding: utf-8 -*-
"""
Created on Thu Oct 30 12:17:44 2014

@author: GiovanniPi
"""

from time import sleep

from PyQt4 import QtGui, Qt

from LoadPull.Drivers.ZNB import ErroriZNB
from TouchstoneS2P import TouchstoneS2P


class ToolFrequenza(QtGui.QDialog):
    
    converter = {"GHz":1000000000, "MHz":1000000, "KHz":1000, "Hz":1}    
    
    def __init__(self, parent = None):
        super(ToolFrequenza, self).__init__(parent)
        sound = Qt.QSound('C:/Users/GiovanniPi/Documents/guidamobile2/LoadPull/Sound/fine_misura.wav',self)
        sound.play()
        self.frequenza = ""
        
        self.setWindowTitle("Selector Frequenza")
        self.combo_unita = QtGui.QComboBox()
        self.combo_unita.addItems(["GHz","MHz","KHz","Hz"])

        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                     QtGui.QDialogButtonBox.Cancel)
        self.edit_freq = QtGui.QLineEdit()
        button_box.accepted.connect(self.ok)
        button_box.rejected.connect(self.reject)
                     
        main_grid = QtGui.QGridLayout()
        main_grid.addWidget(QtGui.QLabel("Inserisci una frequenza"), 0, 0, 1, 3)
        main_grid.addWidget(self.edit_freq, 1, 0, 1, 3)
        main_grid.addWidget(self.combo_unita, 2, 0, 1, 3)
        main_grid.addWidget(button_box, 3, 0, 1, 3)
        
        self.setLayout(main_grid)
        
    def ok(self):
        try:
            value = float(self.edit_freq.text())
            unita = str(self.combo_unita.currentText())
            self.frequenza = value * self.converter.get(unita)
            assert self.frequenza > 550000000
            assert self.frequenza < 2700000000
            self.accept()
        except (ValueError, AssertionError):
            QtGui.QMessageBox.warning(self, "Errore", 
                                     """Inserisci un valore per la frequenza 
                                     compreso fra 550MHz e 2.7GHz""")

    def retFrequenza(self):
        if self.frequenza < 1150000000:
            filtro = "High-Pass 1200"
        else:
            filtro = "High-Pass 3400"
        return self.frequenza, filtro


class ToolLimiti(QtGui.QDialog):

    def __init__(self, low, high, parent = None):
        super(ToolLimiti, self).__init__(parent)
        self.setWindowTitle("Setta limiti")
        
        self.label_min = QtGui.QLabel(str(low))
        self.label_max = QtGui.QLabel(str(high))
     
        hor_box_min = QtGui.QHBoxLayout()
        hor_box_min.addWidget(QtGui.QLabel("Min angle: "))
        hor_box_min.addWidget(self.label_min)
        hor_box_min.addStretch()
        
        hor_box_max = QtGui.QHBoxLayout()
        hor_box_max.addWidget(QtGui.QLabel("Max angle: "))
        hor_box_max.addWidget(self.label_max)
        hor_box_max.addStretch()

        self.slider_min = self.creaSlider(0, high, low, self.printMin, 
                                          self.minReleased)
        self.slider_max = self.creaSlider(low, 360, high, self.printMax, 
                                          self.maxReleased)
        label_min_min = QtGui.QLabel("0")
        self.label_min_max = QtGui.QLabel(str(high))
        self.label_max_min = QtGui.QLabel(str(low))
        label_max_max = QtGui.QLabel("360")
        
        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|
                                            QtGui.QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.ok)
        button_box.rejected.connect(self.reject)
        
        grid = QtGui.QGridLayout()
        grid.addLayout(hor_box_min, 0, 1, 1, 3)
        grid.addWidget(label_min_min, 1, 0, 1, 1)
        grid.addWidget(self.label_min_max, 1, 4, 1, 1)
        grid.addWidget(self.slider_min, 1, 1, 1, 3)
        grid.addLayout(hor_box_max, 3, 1, 1, 3)
        grid.addWidget(self.label_max_min, 4, 0, 1, 1)
        grid.addWidget(label_max_max, 4, 4, 1, 1)
        grid.addWidget(self.slider_max, 4, 1, 1, 3)
        grid.addWidget(button_box, 6, 2, 1, 3)
        
        self.setLayout(grid)

    def creaSlider(self, low, high, init_pos, on_changed, on_released):
        slider = QtGui.QSlider()
        slider.setRange(low/10, high/10)
        slider.setValue(init_pos/10)
        slider.setOrientation(Qt.Qt.Horizontal)
        slider.setTickPosition(2)
        slider.setTickInterval(1)
        slider.setMinimumWidth(200)
        slider.valueChanged.connect(on_changed)
        slider.sliderReleased.connect(on_released)
        return slider
       
    def printMax(self, value):
        self.label_max.setText(str(value*10))

    def printMin(self, value):
        self.label_min.setText(str(value*10))
        
    def minReleased(self):
        value = int(self.label_min.text())/10
        self.slider_max.setMinimum(value)
        self.label_max_min.setText(str(value*10))
        
    def maxReleased(self):
        value = int(self.label_max.text())/10
        self.slider_min.setMaximum(value)
        self.label_min_max.setText(str(value*10))
        
    def ok(self):
        self.accept()
        
    def retAngles(self):
        return int(self.label_max.text()), int(self.label_min.text())
        

class ToolBand(QtGui.QDialog):
    """This dialog ask for a test band. The association between bands and channel are
    umts1: 9750
    umts2: 9400
    umts4: 1412
    umts5: 4182
    umts8: 2787
    """
    #return infromation assiociated to the band [tx frequency, filter, technology]
    bandToFreq = {"GSM 850":[836000000.0, "High-Pass 1200", (0,1)],
                   "GSM 900":[896000000.0, "High-Pass 1200", (0,1)],
                   "GSM 1800":[1747800000.0, "High-pass 3400", (0,1)],
                   "GSM 1900":[1880000000.0, "High-pass 3400", (0,1)],
                   "UMTS 1":[1950000000.0, "High-pass 3400", (1)],
                   "UMTS 2":[1880000000.0, "High-pass 3400", (1)],
                   "UMTS 4":[1732400000.0, "High-pass 3400", (1)],
                   "UMTS 5":[836400000.0, "High-Pass 1200", (1)],
                   "UMTS 8":[897400000.0, "High-Pass 1200", (1)]}
    
    def __init__(self, parent, tecnologia):
        super(ToolBand, self).__init__(parent)
        self.setWindowTitle("Seleziona banda")
        self.tecnologia = tecnologia
        self.comboBanda = QtGui.QComboBox()
        self.comboBanda.addItems(["GSM 850","GSM 900","GSM 1800","GSM 1900", "UMTS 1", "UMTS 2", "UMTS 4", "UMTS 5",
                                  "UMTS 8", "LTE"])
        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok| QtGui.QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)
        vbox_layout = QtGui.QVBoxLayout()
        vbox_layout.addWidget(QtGui.QLabel("Select a band"))
        vbox_layout.addWidget(self.comboBanda)
        vbox_layout.addWidget(button_box)
        self.setLayout(vbox_layout)

    @property
    def frequency(self):
        """Return the frequency to be used, the band and the flag indicating if the cmu connected can handle the
        technology requested"""
        key = str(self.comboBanda.currentText())
        a = self.bandToFreq.get(key)
        if self.tecnologia in a[2]:
            cmu = True
        else:
            cmu = False
        #[frequenza, filtro, unused], nome_tecnologia, bool cmu
        return a, key, cmu
        

class CalibrazioneCavi(QtGui.QDialog):
    
    def __init__(self, parent = None):
        try:
            super(CalibrazioneCavi, self).__init__(parent)
            self._freq_list = []
#            self._znb = ZNB()
            self._s2p = TouchstoneS2P()
            self.setWindowTitle("Acquire cable")
            self.edit_name = QtGui.QLineEdit()
            self.edit_comment = QtGui.QLineEdit()
            btn_acquire = QtGui.QPushButton("Save")
            btn_acquire.clicked.connect(self.save)
            btn_exit = QtGui.QPushButton("Exit")
            btn_exit.clicked.connect(self.close)
            hbox = QtGui.QHBoxLayout()
            hbox.addWidget(btn_acquire)
            hbox.addWidget(btn_exit)
            grid = QtGui.QGridLayout()
            grid.addWidget(QtGui.QLabel("Insert file name"), 0, 0, 1, 1)
            grid.addWidget(self.edit_name, 0, 1, 1, 2)
            grid.addWidget(QtGui.QLabel("Insert comment"), 1, 0, 1, 1)
            grid.addWidget(self.edit_comment, 2, 0, 1, 3)
            grid.addLayout(hbox, 3, 0, 1, 3)
            self.setLayout(grid)
#            self.setSpettro()
        except ErroriZNB as e:
            raise ErroriZNB(e)
            
    def save(self):
        try:
            comment = str(self.edit_comment.text())
            name = str(self.edit_name.text())
            assert name, comment
            re11 = []
            im11 = []
            re21 = []
            im21 = []
            re22 = []
            im22 = []
            re12 = []
            im12 = []
            re11, im11 = self._znb.readTrace(1)
            sleep(0.3)
            re21, im21 = self._znb.readTrace(2)
            sleep(0.3)
            re22, im22 = self._znb.readTrace(4)
            sleep(0.3)
            re12, im12 = self._znb.readTrace(3)
            sleep(0.3)
            self._s2p.name = name
            self._s2p.comment = comment
            self._s2p.write(self._freq_list, re11, im11, re21, im21, re12, im12,
                            re22, im22)
        except AssertionError:
            return

    def setSpettro(self):
        self._znb.points = 1001
        self._znb.start = "500 MHz"
        self._znb.stop = "5 GHz"
        for i in xrange(1,5):
            self._znb.setTrace(i)
        self._freq_list = self._znb.listaFrequenze
        
if __name__ == "__main__":
    app = QtGui.QApplication([])
    form = CalibrazioneCavi()
    form.show()
    app.exec_()