__author__ = 'GiovanniPi'

"""Tool for tuner calibration"""


from ..TouchstoneS2P import TouchstoneS2P
from ..Drivers.ZNB import ZNB
from PyQt4 import QtGui, QtCore
import ui_Line_calibration


class CalibrazioneGUI(QtGui.QDialog, ui_Line_calibration.LineCalibration):

    current_pos = 0
    limite_giri = 70000
    passo = 246

    def __init__(self, motore, rele, parent = None):
        super(CalibrazioneGUI, self).__init__(parent)
        self.setupUi(self)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.onTimeout)
        self.motore = motore
        self.rele = rele
        self.touch_file = TouchstoneS2P()
        self.znb = ZNB()

    def onStart(self):
        """Lancia la misura"""
        self.setPath()
        self.current_pos = 0
        self.inPosition()      #Setta il contatore a 0, legge la prima posizione (0)

    def onStop(self):
        """Interrompe la calibrazione in corso, per farla ripartire va dato lo start"""
        self.enableBtns(True)  #attiva il pulsante e ferma il timer
        self.timer.stop()

    def onReset(self):
        """Reset della guida"""
        self.rele.reset()

    def updateBar(self, value):
        self.progress_bar.setValue(int(value/284*100))

    def onTimeout(self):
        """Controllo stato motori"""
        response = self.motori.read() #lettura risposta scheda
        try:
            if response:
                clean = response.strip()  #pulisce stinga da /n/r
                if clean == "D":          #D indica task completata
                    self.timer.stop()
                    self.inPosition()
                else:                     #qualsiasi altro carattere implica o errore o numeri giri per fine guida minore
                    self.endTask('End before limit (%s)' % clean, True)
            return None                   #None indica la guida sta ancora muovendosi, parte con prossimo timer
        except Exception:
            self.timer.stop()
            self.endTask('Error during the test, please restart it', True)

    def inPosition(self):
        """Routine per la lettura dei dati e selezione prossimo ciclo"""
        self.readData()
        self.current_pos += self.passo     #incrementa variabile prossima posizione in base al passo selezionato
        if self.current_pos < self.limite_giri:
            self.motore.goTo(str(self.current_pos))   #attiva movimento guida
            self.timer.start(500)                     #inizia ciclo di controllo
        else:
            self.endTask('Path calibrated')           #fine ciclo calibrazione

    def readData(self):
        pass

    def endTask(self, message, error = False):
        """Routine di fine task. Viene visualizzato un messaggio di fine misura o di errore"""
        if error:
            QtGui.QMessageBox.warning(self, 'Warning', message)         #errore durante la misura, mostra errore
        else:
            QtGui.QMessageBox.information(self, 'Message', message)     #mostra messaggio di fine misura
            self.enableBtns(True)

    def enableBtns(self, flag):
        """Se True attiva lo start e disabilita lo stop, altrimenti fa l'opposto"""
        self.start_btn.setEnabled(flag)
        self.stop_btn.setEnabled(not flag)

    def setPath(self):
        swr = str(self.combo_swr.text())
        filtro = str(self.combo_filtri.text())[-4:]
        porta = str(self.combo_porta.text())
        self.rele.setFiltro(filtro)
        self.rele.setSwr(swr)
        pass
