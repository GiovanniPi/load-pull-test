# -*- coding: utf-8 -*-
"""
Created on Fri Nov 07 11:29:57 2014

@author: GiovanniPi
"""

class TouchstoneS2P(object):

    #intestazione dei parametri per fiel touchstone
    paramsOrder = "!freq   ReS11    ImS11   ReS21    ImS21   ReS12    ImS12\
    ReS22    ImS22"
    options = "#GHz S   RI  R   50"

    def __init__(self):
        self._name = ""
        self._comment = []


    @property
    def name(self):
        """Nome di salvataggio del file"""
        print self._name

    @name.setter
    def name(self, name):
        if ".s2p" not in name:
            name = name + ".s2p"
        self._name = name

    @property
    def comment(self):
        """Parte di commenti iniziali del file s2p"""
        print self._comment

    @comment.setter
    def comment(self, comment):
        if not comment.startswith("!"):
            comment = "!"+comment
        self._comment = comment

    def read(self):
        with open(self._name, 'r') as f:
            for lines in f:
                print lines
    
    def write(self, *args):
        with open(self._name, 'w') as f:
            f.write(self._comment + "\n")
            f.write(self.options + "\n")
            f.write(self.paramsOrder + "\n")
            
            for index, value in enumerate(args[0]):
                stringa = "%0.5f %0.5f %0.5f %0.5f %0.5f %0.5f %0.5f %0.5f %0.5f" \
                            % (value, args[1][index], args[2][index],
                               args[3][index], args[4][index], args[5][index],
                               args[6][index], args[7][index], args[8][index])
                f.write(stringa + "\n")
                
    def invertiColonne(self, old):
        invalid = '#','!'
        with open(old, 'r') as old_f:
            with open(self._name, 'w') as new:
                new.write(self._comment+ '\n')
                new.write(self.options + '\n')
                new.write(self.paramsOrder + '\n')
                for lines in old_f:
                    if lines.startswith(invalid):
                        continue
                    linea = lines.split()
                    new_line = " ".join([linea[0], linea[2], linea[1], linea[4],
                                         linea[3], linea[6], linea[5], linea[8],
                                         linea[7]])
                    new.write(new_line + '\n')