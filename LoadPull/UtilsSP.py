# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 16:52:15 2014

@author: GiovanniPi
"""

import numpy as np


class WrapperParams(object):
    """Container for the test parameters"""
    
    def __init__(self):
        self.__swr = None
        self.__filter = None
        self.__pos = None
        self.__trans_a = None
        self.__trans_b = None
        self.__trans_c = None
        self.__trans_cwg = None
        self.__save = r'C:\Users\emclab1\Desktop\LP_Data\\'
        self.__band = None
        self.__points = []
        #Test tristate variables: None, not created, True selected, False deselected
        self.__harmonic = None
        self.__power = None
        self.__consumption = None
        self.__sensibility = None
        self.__power_fsv = None
        self.__session_folder = None
        self.__radiation = None
    
    @property
    def saveDir(self):
        """Destinazione di salvataggio file"""
        return self.__session_folder
        
    @saveDir.setter
    def saveDir(self, folder):
        self.__session_folder = self.__save + folder
    
    @property
    def pos(self):
        """Parametro posizione guida"""
        return self.__pos
    
    @pos.setter
    def pos(self, pos):
        self.__pos = pos
    
    @property
    def swr(self):
        """Parametro carico selezionato"""
        return self.__swr
    
    @swr.setter
    def swr(self, swr):
        self.__swr = swr
    
    @property
    def filtro(self):
        """Parametro filtro selezionato"""
        return self.__filter
        
    @filtro.setter
    def filtro(self, filtro):
        self.__filter = filtro
    
    @property
    def frequency(self):
        """Parametro frequenza di lavoro"""
        return self.__freq
        
    @frequency.setter
    def frequency(self, value):
        self.__freq = int(value)
    
    @property
    def transA(self):
        """Contiene il file s2p per caratterizzare il cavo collegato alla porta
        A"""
        return self.__trans_a

    def setTransA(self, path):
        self.__trans_a = path
        
    @property
    def transB(self):
        """Contiene il file s2p per caratterizzare il cavo collegato alla porta
        B"""
        return self.__trans_b

    def setTransB(self, path):
        self.__trans_b = path

    @property
    def transC(self):
        """Contiene il file s2p per caratterizzare il cavo collegato alla porta
        C"""
        return self.__trans_c
    
    def setTransC(self, path):
        self.__trans_c = path

    @property
    def transCwg(self):
        """Contiene il file s2p per caratterizzare il cavo collegato alla porta
        C"""
        return self.__trans_cwg

    def setTransCwg(self, path):
        self.__trans_cwg = path
        
    @property
    def band(self):
        return self.__band
        
    @band.setter
    def band(self, value):
        """Banda salvato come GSM numero o UMTS numero"""
        self.__band = value

    @property
    def armoniche(self):
        return self.__harmonic
    
    @armoniche.setter
    def armoniche(self, status):
        self.__harmonic = status

    @property
    def potenza(self):
        return self.__power
    
    @potenza.setter
    def potenza(self, status):
        self.__power = status
        
    @property
    def sensibilita(self):
        return self.__sensibility
    
    @sensibilita.setter
    def sensibilita(self, status):
        self.__sensibility = status

    @property
    def consumi(self):
        return self.__consumption
    
    @consumi.setter
    def consumi(self, status):
        self.__consumption = status
        
    @property
    def potfsv(self):
        return self.__power_fsv
    
    @potfsv.setter
    def potfsv(self, status):
        self.__power_fsv = status

    @property
    def rad(self):
        return self.__radiation

    @rad.setter
    def rad(self, status):
        self.__radiation = status

    def checkReady(self, session):
        """:param session: name of the session"""
        if session:
            self.saveDir = session
        else:
            self.saveDir = 'No name'
        if not self.__filter:
            raise ValueError("Devi settare una banda")
        if not self._freq:
            raise ValueError("Inserisci una frequenza o una banda da misurare")

    @property
    def vettore_riduzione(self):
        return self.__points
    
    @vettore_riduzione.setter
    def vettore_riduzione(self, vettore):
        del self.__points[:]
        self.__points = vettore[:]
    

"""
__author__ = MicheleKo
"""
class Matrici(object):
    
    def __init__(self):
        pass

    def creaMatrice(self, stringa_s2p, type_m = "s"):
        lista = stringa_s2p.split()
        matrice = np.matrix([[0.0 + 1j, 0.0 + 1j],[0.0 + 1j, 0.0 + 1j]])
        matrice[0,0] = float(lista[1]) + float(lista[2])*1j #s11
        matrice[1,0] = float(lista[3]) + float(lista[4])*1j #s21
        matrice[0,1] = float(lista[5]) + float(lista[6])*1j #s12
        matrice[1,1] = float(lista[7]) + float(lista[8])*1j #s22
        if type_m == 's':
            return matrice
        return self.s_to_t(matrice)

    def det(self, matrice):
        """
        funzione per calcolare il determinante della matrice
        """
        return np.linalg.det(matrice)
        
    def nonZero(self, matrice):
        """
        funzione per verificare che la funzione sia invertibile e che non contenga valori nulli
        """
        zero=1
        while(zero):
            if matrice[0,0]==0 or matrice[0,1]==0 or matrice[1,0]==0 or matrice[1,1]==0:
                while(matrice[0,0]==0 or matrice[0,1]==0 or matrice[1,0]==0 or matrice[1,1]==0):
                    matrice[0,0] = matrice[0,0] + 1.1E-5
                    matrice[0,1] = matrice[0,1] + 1.2E-5
                    matrice[1,0] = matrice[1,0] + 1.3E-5
                    matrice[1,1] = matrice[1,1] + 1.4E-5
                    if matrice[0,0]==0 or matrice[0,1]==0 or matrice[1,0]==0 or matrice[1,1]==0:
                        zero = 1
                    else: zero = 0
            else: zero = 0
        return matrice

    def inversa(self, matrice):
        """
        funzione per calcolare l'inversa della matrice
        """
        try: 
            return np.matrix.getI(matrice)
        except Exception:
            matrice[0,0] += 1.1E-5
            matrice[0,1] += 1.2E-5
            matrice[1,0] += 1.3E-5
            matrice[1,1] += 1.4E-5
            return matrice

    def s_to_t(self, matrice):
        """
        funzione per convertire la matrice s nella matrice t
        """
        self.nonZero(matrice)
        matrice_t = np.matrix([[0.0 + 1j, 0.0 + 1j],[0.0 + 1j, 0.0 + 1j]])
        matrice_t[0,0] = 1/matrice[1,0]
        matrice_t[0,1] = -matrice[1,1]/matrice[1,0]
        matrice_t[1,0] = matrice[0,0]/matrice[1,0]
        matrice_t[1,1] = -self.det(matrice)/matrice[1,0]
        return matrice_t

    def t_to_s(self, matrice):
        """
        funzione per convertire la matrice t nella matrice s
        """
        self.nonZero(matrice)
        matrice_s = np.matrix([[0.0 + 1j, 0.0 + 1j],[0.0 + 1j, 0.0 + 1j]])
        matrice_s[0,0] = matrice[1,0]/matrice[0,0]
        matrice_s[0,1] = self.det(matrice)/matrice[0,0]
        matrice_s[1,0] = 1/matrice[0,0]
        matrice_s[1,1] = -matrice[0,1]/matrice[0,0]
        return matrice_s
