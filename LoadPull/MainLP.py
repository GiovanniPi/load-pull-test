# -*- coding: utf-8 -*-

__author__ = 'GiovanniPi'
__version__ = 'Pene1'

from PyQt4 import QtGui, QtCore
from PyQt4.Qt import QSound
from Misure import Armoniche, Consumi, Pot, Sens, PotFSV, ErroriMisura, Radiazione
from Drivers.DriverRelay import DriverRelay, ErrorRelay
from Drivers.DriverMotor import DriverMotor, ErrorMotor
from calibrationMode import CalibrazioneGUI
from UtilsSP import WrapperParams
from itertools import cycle
from Tools import ToolBand, CalibrazioneCavi
from os import startfile
import ui_LoadPull


def check_pbar(f):
        def wrapper(*args):
            if args[0].lbl_status.text() == 'Testing':
                sound = QSound('Sound/no_edit.wav', args[0])
                sound.play()
                QtGui.QMessageBox.warning(args[0], "Warning", "Can't modify the test while it's running")
            else:
                sound = QSound('Sound/edit_param.wav', args[0])
                sound.play()
                return f(*args)
        return wrapper


class ErrorLP(Exception):

    def __init__(self, message):
        self._message = message

    def __str__(self):
        return repr(self._message)


# noinspection PyUnresolvedReferences,PyCallByClass,PyTypeChecker,PyArgumentList
class MainGUI(QtGui.QMainWindow, ui_LoadPull.Ui_LoadPull):
    #Parametri fissi
    mm_giro = 0.002034 #mm per giro motore
    v_luce = 299792458 #m/s

    def __init__(self, parent = None):
        try:
            super(MainGUI, self).__init__(parent)
            self.setupUi(self)
            self.wrapper_par = WrapperParams()
            self.rele = DriverRelay()
            self.motors = DriverMotor()
            #must detect the type of cmu connected
            self.mis_pot = Pot(self.wrapper_par)
            #indicates the cmu type: -1-no CMU; 0-only GSM; 1-UMTS
            self.__cmu_type = self.mis_pot.cmuType
            self.flag_cmu = True
        except ErroriMisura:
            self.flag_cmu = False
            self.__cmu_type = -1
        except (ErrorMotor, ErrorRelay) as e:
            QtGui.QMessageBox.warning(self, 'Error', str(e))
        self.full_test = False
        #indicates if the last command requested was a reset
        self.resetting = False
        #indicates if the cmu is present or if connected and able to work with a particular technology
        #positions vector
        self.vettore_posizione = []
        self._frequenza = ""
        self.swr_iterator = None
        #length of the maximum escursion
        self._lambda_quarti = 0
        self._counter = 0.0
        self._cycle = None
        self.lbl_status = QtGui.QLabel("Ready")
        self.p_bar = QtGui.QProgressBar(self)
        self.p_bar.setRange(0, 100)
        self.p_bar.setFixedWidth(100)
        self.p_bar.setValue(0)
        self.statusbar.addPermanentWidget(self.lbl_status)
        self.statusbar.addPermanentWidget(self.p_bar)
        self.timer_motori = QtCore.QTimer(self)
        self.timer_motori.timeout.connect(self.checkMotori)
        self.last_test = None
        self.tipo_riduzione = 1

        reg_ex = QtCore.QRegExp('^[^:"?<>\^\\\/|*]+$')
        valid_session = QtGui.QRegExpValidator(reg_ex)
        self.edit_name.setValidator(valid_session)
        #serve per la progress bar
        self._numero_operazioni = 0

    def enableMenu(self, flag):
        self.act_power.setEnabled(flag)
        self.act_sensibility.setEnabled(flag)
        self.act_power_FSV.setEnabled(not flag)
        if not flag:
            self.act_power.setChecked(False)
            self.act_sensibility.setChecked(False)
            self.wrapper_par.sensibilita = False
            self.wrapper_par.potenza = False
        else:
            self.act_power_FSV.setChecked(False)
            self.wrapper_par.potfsv = False

    @check_pbar
    def onBandSelected(self):
        try:
            dialog = ToolBand(self, self.__cmu_type)
            if dialog.exec_():
                vett, band, self.flag_cmu = dialog.frequency
                """ """
                self.enableMenu(self.flag_cmu)
                if self.flag_cmu:
                    self.mis_pot.settaBanda(band)
                self._frequenza = vett[0]
                self.rele.setFiltro(vett[1])
                self._lambda_quarti = self.v_luce/self._frequenza * 0.25 * 1000
                self.caricaValori(self._lambda_quarti)
                self.updateLogList("Band selected " + band)
                self.updateLogList("New positions loaded")
                self.updateParams(band)
                self.wrapper_par.frequenza = self._frequenza
                self.wrapper_par.filtro = vett[1]
                self.wrapper_par.band = band
                self.graph.clearPlot()
        except ErroriMisura as e:
            sound = QSound('Sound/no_edit.wav', self)
            sound.play()
            QtGui.QMessageBox.warning(self, 'Errore', str(e))

    @check_pbar
    def onCableCal(self):
        dialog = CalibrazioneCavi(self)
        dialog.exec_()

    def onReset(self):
        try:
            done = self.motors.reset()
            if done:
                self.updateLogList("Reset motori")
                self.lbl_status.setText("Resetting")
                self.caricaValori(self._lambda_quarti)
                self.timer_motori.start(500)
                self.resetting = True
        except ErrorMotor as e:
            QtGui.QMessageBox.warning(self, 'Error', str(e))

    def onStart(self):
        """Called on start button clicked"""
        try:
            self.wrapper_par.checkReady(str(self.edit_name.text()))
            load = 1
            if self.full_test:
                self.setSWR(self.swr_iterator.next())
                load = 8
            #in ogni caso poi si passa ad onStart per creare il vettore step di misura
            sound = QSound('Sound/start_misura.wav', self)
            sound.play()
            self._counter = 0.0
            self.startTest(True, load)
            # l'errore lanciato da checkReady e' un ValueError
        except ValueError as e:
            QtGui.QMessageBox.warning(self, "Attenzione", str(e))

    def onSwrSelected(self, btn):
        """Detect if full test or a single swr is selected
        :param btn: the radio button clicked"""
        value = str(btn.text())
        if value == 'Full Test':
            self.full_test = True
            self.swr_iterator = iter(["1","1.5","2","3","4","6","10","Open"])
        else:
            self.full_test = False
            self.setSWR(value)

    @check_pbar
    def onTransducerSelected(self):
        sender = self.sender().text()
        if sender == "Port A":
            string = "Select a transducer for port A"
            setter = self.wrapper_par.setTransA
        elif sender == "Port B":
            string = "Select a transducer for port B"
            setter = self.wrapper_par.setTransB
        elif sender == "Port C":
            string = "Select a transducer for port C"
            setter = self.wrapper_par.setTransC
        else:
            string = "Select a transducer for CWG"
            setter = self.wrapper_par.setTransCwg
        fname = QtGui.QFileDialog.getOpenFileName(self, string, ".", "*.s2p")
        if fname:
            setter(str(fname))
            self.updateTransducer(string[-1], fname)

    def onTestSelected(self, status):
        sender = self.sender()
        try:
            if self.lbl_status.text() == 'Testing':
                raise ErroriMisura("Can't modified the configuration while testing")
            elif sender.text() == "Harmonics":
                if self.wrapper_par.armoniche is None:
                    self.mis_armoniche = Armoniche(self.wrapper_par)
                    self.thread_armoniche = QtCore.QThread()
                    self.mis_armoniche.moveToThread(self.thread_armoniche)
                    self.mis_armoniche.armonica_done.connect(self.plotPoint)
                    self.thread_armoniche.started.connect(self.mis_armoniche.start)
                self.wrapper_par.armoniche = status
                self.updateLogList("Harmonics test selected" if status else "Harmonics test deselected")
            elif sender.text() == "Power":
                if self.wrapper_par.potenza is None:
                    self.thread_potenza = QtCore.QThread()
                    self.mis_pot.moveToThread(self.thread_potenza)
                    self.mis_pot.potenza_done.connect(self.plotPoint)
                    self.thread_potenza.started.connect(self.mis_pot.startPot)
                self.wrapper_par.potenza = status
                self.updateLogList("Power test selected" if status else "Power test deselected")
            elif sender.text() == "Sensibility":
                if self.wrapper_par.sensibilita is None:
                    self.mis_sens = Sens(self.wrapper_par, self.mis_pot.cmu)
                    self.thread_sens = QtCore.QThread()
                    self.mis_sens.moveToThread(self.thread_sens)
                    self.mis_sens.sens_done.connect(self.plotPoint)
                    self.thread_sens.started.connect(self.mis_sens.startSens)
                self.wrapper_par.sensibilita = status
                self.updateLogList("Sensibility test selected" if status else "Sensibility test deselected")
            elif sender.text() == 'Consumption':
                if self.wrapper_par.consumi is None:
                    self.mis_consumi = Consumi(self.wrapper_par)
                    self.thread_consumi = QtCore.QThread()
                    self.mis_consumi.moveToThread(self.thread_consumi)
                    self.mis_consumi.consumi_done.connect(self.plotPoint)
                    self.thread_consumi.started.connect(self.mis_consumi.start)
                self.wrapper_par.consumi = status
                self.updateLogList("Consumption test selected" if status else "Consumption test deselected")
            elif sender.text() == 'Power FSV':
                if self.wrapper_par.potfsv is None:
                    self.mis_potfsv = PotFSV(self.wrapper_par)
                    self.thread_potfsv = QtCore.QThread()
                    self.mis_potfsv.moveToThread(self.thread_potfsv)
                    self.mis_potfsv.potenza_fsv_done.connect(self.plotPoint)
                    self.thread_potfsv.started.connect(self.mis_potfsv.leggiPotenza)
                self.wrapper_par.potfsv = status
                self.updateLogList("Power test selected [FSV method]" if status else "Power test deselected [FSV method]")
            else:
                if self.wrapper_par.rad is None:
                    self.mis_rad = Radiazione(self.wrapper_par)
                    self.thread_rad = QtCore.QThread()
                    self.mis_rad.moveToThread(self.thread_rad)
                    self.mis_rad.radiazione_done.connect(self.plotPoint)
                    self.thread_rad.started.connect(self.mis_rad.start)
                self.wrapper_par.rad = status
                self.updateLogList("Radiation test selected" if status else 'Radiation test deselected')
        except ErroriMisura as e:
            #if trying to modify the tests lst while runnning, the modification will be rejected
            sender.toggled[bool].disconnect(self.onTestSelected)
            sender.setChecked(not status)
            QtGui.QMessageBox.warning(self, "Warning", unicode(e))
            sender.toggled[bool].connect(self.onTestSelected)

    @check_pbar
    def onTunerCal(self):
        """Calibration routine for the tuner"""
        dialog = CalibrazioneGUI(self.motors, self.rele, self)
        dialog.exec_()

    def plotPoint(self, lis):
        """Called when a thread is fished. Close the correct thread, plot the last point tested, show the value
        on screen.
        :param lis: vector """
        if lis[1] == 'Consumi':
            self.thread_consumi.quit()
        elif lis[1] == 'Potenza':
            self.thread_potenza.quit()
        elif lis[1] == 'Sensibilita':
            self.thread_sens.quit()
        elif lis[1] == 'Harmonics':
            self.thread_armoniche.quit()
        elif lis[1] == 'Potenza FSV':
            self.thread_potfsv.quit()
        else:
            self.thread_rad.quit()
        if lis[0]:
            self.lbl_last_point.setText(self.graph.addPoint(lis[2], lis[1], lis[3]))
            message = ''
        else:
            message = lis[2]
        #valore del punto complesso, nome misura, valore misurato
        #se lis[0] = True indica che la misura e' stata completata e il dato
        #da misurare e' pronto. In ogni cosa, rilancio onThreadEnd, con lo status della misura
        self.onThreadEnd(lis[0], message)

    def setSWR(self, value):
        """:param value: set the new SWR"""
        message, vector = self.rele.setSwr(value, self.tipo_riduzione)
        self.updateLogList(message)
        self.wrapper_par.swr = value
        self.wrapper_par.vettore_riduzione = vector

    def startTest(self, prima = False, carichi = 8):
        """Carica il vettore di misura, ed esegue i controlli sulle folder dei
        singoli test selezionati. Se prima e' True calcola anche il numero
        di operazioni da eseguire per aggiornare la progress bar, carichi indica
        il numero di carichi da calcolare: 8-> full test, 1-> singolo carico"""
        v = [self.nextPos]
        if self.flag_cmu:
            v.append(self.controlloLink)
        if self.wrapper_par.armoniche:
            self.mis_armoniche.beforeStart()
            self.mis_armoniche.checkFolder()
            v.append(self.thread_armoniche.start)
        if self.wrapper_par.consumi:
            self.mis_consumi.checkFolder()
            v.append(self.thread_consumi.start)
        if self.wrapper_par.potenza:
            self.mis_pot.checkFolderPot()
            v.append(self.thread_potenza.start)
        if self.wrapper_par.potfsv:
            self.mis_potfsv.checkFolder()
            v.append(self.thread_potfsv.start)
        if self.wrapper_par.sensibilita:
            self.mis_sens.stopBer()
            self.mis_sens.checkFolderSens()
            v.append(self.thread_sens.start)
        if self.wrapper_par.rad:
            self.mis_rad.beforeStart()
            self.mis_rad.checkFolder()
            v.append(self.thread_rad.start)
        if prima:
            self._numero_operazioni = (len(v) * len(self.vettore_posizione) - 1.0) * carichi
        rid = self.wrapper_par.vettore_riduzione
        vettore_punti = [self.vettore_posizione[i] for i in range(len(self.vettore_posizione)) if rid[i] is not 0]
        self.iter_posizioni = iter(vettore_punti)
        self.lbl_status.setText("Testing")
        self.enableWidget(False)
        if self._cycle:
            del self._cycle
        self._cycle = cycle(v)
        #associo la porssima azione alla variabile last text, così viene
        #salvata e rilanciata in caso di errori di misura
        self.last_test = self._cycle.next()
        self.last_test()

#----------------------------------------------------------------------------------------------------
    def onSelezionaRiduzione(self, status):
        try:
            if self.lbl_status.text() == 'Testing':
                raise ErroriMisura("Non puoi effettuare modifiche a misura in corso")
            sender = self.sender()
            if sender.text() == 'No riduzione':
                self.tipo_riduzione = 0
            elif sender.text() == 'Riduzione':
                self.tipo_riduzione = 1
            else:
                self.tipo_riduzione = 2
        except ErroriMisura as e:
            #se lanciata eccezione, opzione selezionata rimane nello stato precedente
            sender.toggled[bool].disconnect(self.onSelezionaRiduzione)
            sender.setChecked(not status)
            QtGui.QMessageBox.warning(self, "Attenzione", unicode(e))
            sender.toggled[bool].connect(self.onSelezionaRiduzione)

    def onThreadEnd(self, status, message = ''):
        """Non piu' lanciata alla chiusura del thread, ma richiamata dopo
        riposizionamento, controllo link positivo e plot. Lo status indica se 
        misura e' stata eseguita correttamente, altrimenti da errore e permette
        uscita della misura o la ripetizione dell'ultimo test eseguito"""
        if status:
            self.last_test = self._cycle.next()
            self.last_test()
            self._counter += 1
            self.updateBar()
        else:
            scelta = QtGui.QMessageBox.information(self, "Attenzione", 
                    "%s oppure Reset per fermare la misura in corso" % message,
                    QtGui.QMessageBox.Ok|QtGui.QMessageBox.Cancel,
                    QtGui.QMessageBox.Ok)
            if scelta == QtGui.QMessageBox.Ok:
                self.last_test()
            else:
                self.enableWidget(True)
                self.lbl_status.setText('Ready')

    def nextPos(self):
        try:
            self.resetting = False
            next_pos = self.iter_posizioni.next()
            self.wrapper_par.pos = next_pos
            if next_pos == 0:
                self.updateLogList("Next position: " + str(next_pos))
                self.onThreadEnd(True)
            else:
                status = self.motors.goTo(str(next_pos))
                if status:
                    self.updateLogList("Next position: " + str(next_pos))
                    self.timer_motori.start(500)
        except ErrorLP as e:
            QtGui.QMessageBox.warning(self, "Message", str(e))
        except StopIteration:
            if self.full_test:
                self.onReset()
                return
            sound = QSound('Sound/fine_misura.wav', self)
            sound.play()
            QtGui.QMessageBox.information(self, "Message", "Test completed")
            self.lbl_status.setText("Ready")
            self.enableWidget(True)

    def checkMotori(self):
        response = self.motors.read
        if response:
            clean = response.strip()
            if clean == "D":
                self.timer_motori.stop()
                if not self.resetting:
                    self.onThreadEnd(True)
                else:
                    if self.full_test:
                        try:
                            self.setSWR(self.iteratore_swr.next())
                            self.startTest()
                        except StopIteration:
                            sound = QSound('Sound/fine_misura.wav',
                               self)
                            sound.play()
                            QtGui.QMessageBox.information(self, "Message", "Test completed")
                            self.lbl_status.setText("Ready")
                            self.enableWidget(True)
                    else:    
                        self.p_bar.setValue(0)
                        self.lbl_status.setText("Ready")
                
    def controlloLink(self):
        try:
            self.mis_pot.checkLink()
            self.onThreadEnd(True)
        except ErroriMisura:
            self.onThreadEnd(False, 'Link caduto, ristabilire la chiamate e premere ok, ')
                
    def caricaValori(self, lambda_quarti):
        """Funzione per caricare i valori di posizione della guida. Vengono
        caricati ad ogni variazione della frequenza.
        """
        passo = lambda_quarti / 36.0
        del self.vettore_posizione[:]
        for i in xrange(0, 36):
            next_pos = passo * i
            next_giri = int(next_pos / self.mm_giro)
            self.vettore_posizione.append(int(round(next_giri/246)*246))
#            self.iter_posizioni = iter(self.vettore_posizione)

    def updateTransducer(self, letter, fn):
        fns = fn.split("/")[-1] 
        if letter == 'A':
            self.lbl_a_trans.setText(str(fns))
            return
        elif letter == 'B':
            self.lbl_b_trans.setText(str(fns))
            return
        elif letter == 'C':
            self.lbl_c_trans.setText(str(fns))
            return
        else:
            self.lbl_cwg_trans.setText(str(fns))

    def updateBar(self):
        prog = int(self._counter / self._numero_operazioni * 100)
        self.p_bar.setValue(prog)

    def updateLogList(self, message):
        self.list_log.addItem(str(message))
    
    def updateParams(self, banda = None):
        if self._lambda_quarti:
            self.lbl_lambda.setText( "%0.2f mm" % (self._lambda_quarti * 4))
        if self._frequenza:
            self.lbl_freq.setText("%0.3f MHz" % (self._frequenza/1000000))
        if banda:
            self.lbl_band.setText(banda)
        self.lbl_max.setText('360')
        self.lbl_min.setText('0')

    def enableWidget(self, status):
        self.groupBox_2.setEnabled(status)
        self.btn_start.setEnabled(status)
        self.btn_reset.setEnabled(status)
            
    def onHelp(self):
        startfile('README.md')
        
    def onAbout(self):
        stringa =  """Interact with the Input impedance tuner
        Need PyQt4, pySerial, pyVisa, pyqtgraph installed in the PC."""
        QtGui.QMessageBox.about(self, "Info", stringa)
        
    def closeEvent(self, event):
        self.lbl_status.setText("Closing")
        scelta = QtGui.QMessageBox.question(self, "Domanda....",
                                    "Vuoi resettare la scheda",
                                    QtGui.QMessageBox.Yes|QtGui.QMessageBox.No,
                                    QtGui.QMessageBox.No)
        if scelta == QtGui.QMessageBox.Yes:
            self.rele.setSwr("1")
            self.rele.setFiltro("High-Pass 3400")
        self.motors.closeConnection()
        event.accept()
