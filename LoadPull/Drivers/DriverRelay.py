# -*- coding: utf-8 -*-
"""
Created on Wed Oct 29 11:59:07 2014

@author: GiovanniPi
"""

import visa
import HandlerIndirizzi as HI
from time import sleep


class ErrorRelay(Exception):
    
    def __init__(self, message):
        self._message = message
        
    def __str__(self):
        return repr(self._message)


class DriverRelay(object):
    
    load_converter = {"1.5":"a","2":"b","3":"c","4":"d","6":"e","10":"f",
                     "Open":"g","1":"h","Reset":None} 
    filt_converter = {"High-pass 3400":"l","High-Pass 1200":"m"}
    extra_ridotti = {'1':[1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
               '1.5':[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0],
               '2':[1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0],
               '3':[1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0],
               '4':[1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0],
               '6':[1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0],
               '10':[1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0],
               'Open':[1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0]}
    
    def __init__(self):
        try:
            self._current_filt = None
            self._current_swr = None
            rm = visa.ResourceManager()
            self.board = rm.open_resource(HI.indirizzi.get('rele'))
        except visa.VisaIOError:
            raise ErrorRelay("Scheda rele non connessa")
        
    def setSwr(self, carico, n_punti = None):
        """Attiva un relè parte dei carichi, ritorna stringa per lista log.
        Se carico e' reset, viene solo fatto il reset della scheda.
        In numerp punti passo il controllo di numeri da punti da creare:
        -0 -> tutti i punti
        -1 -> riduzione
        -2 -> numero punti minimo
        """
        string = 'Reset'
        self.resetCarico()
        value = self.load_converter.get(carico)
        if value:
            self.board.write(value)
            sleep(0.5)
            self.board.write(value.upper())
            string = "SWR: " + carico
        self._current_swr = carico
        if n_punti != None:
            v = self.extra_ridotti.get(carico)
        else:
            v = [1] * 36
        return string, v
            
    def resetCarico(self):
        """Resetta il rele 8 porte
        """
        self.board.write("i")
        sleep(0.5)
        self.board.write("I")
        sleep(0.5)
        
    def setFiltro(self, filt):
        """Seleziona il tipo di filtro da usare
        """
        value = self.filt_converter.get(filt)
        if value:
            self.board.write(value)
            sleep(0.5)
            self.board.write(value.upper())
            sleep(0.5)
            self._current_filt = filt
            return "Set filtro: " + filt

    @property
    def returnLoad(self):
        return self.load_converter
        
    @property
    def swr(self):
        return self._current_swr
        
    @property
    def filtro(self):
        return self._current_filt