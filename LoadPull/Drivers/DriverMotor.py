# -*- coding: utf-8 -*-
"""
Created on Tue Oct 28 09:41:54 2014

@author: GiovanniPi
"""
'''Class to interface with the arduino controller, it's based on the DriverArduino.ino'''


version = "2"
SPEED = 9600

import serial
import HandlerIndirizzi
from time import sleep


class ErrorMotor(Exception):
    
    def __init__(self, message):
        self.error = message
        
    def __str__(self):
        return repr(self.error)
        

class DriverMotor(object):

    def __init__(self):
        try:
            port = HandlerIndirizzi.indirizzi.get('motor')
            self.connection = serial.Serial(port, SPEED)
        except serial.SerialException:
            raise ErrorMotor("Motor controller is not connected")
        
    def reset(self):
        """Reset the line by moving to pos = 0"""
        try:
            self.connection.write('r')
            sleep(0.1)
            value = self.connection.read(2)
            assert value == "Ok"
            return True
        except AssertionError:
            print 'Error during the comunication'
            raise ErrorMotor("Error during the communication")
        
    def goTo(self, position):
        try:
            self.connection.write('p')
            sleep(0.1)
            self.connection.write(position)
            sleep(0.1)
            value = self.connection.read(2)
            assert value == "Ok"
            return True
        except AssertionError:
            print 'Error while asking for position'
            raise ErrorMotor("Error during the communication")
            
    @property
    def read(self):
        """Read the motor status"""
        if self.connection.inWaiting() > 0:
            response = self.connection.read(self.connection.inWaiting())
            return response
        return None

    def encoder(self):
        """Read the oncoder (line position)"""
        self.connection.write('e')
        sleep(0.1)
        encoder = self.connection.read(self.connection.inWaiting())
        return encoder

    def closeConnection(self):
        """Close the connection with the board"""
        self.connection.close()
