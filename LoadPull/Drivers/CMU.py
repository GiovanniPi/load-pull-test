# -*- coding: utf-8 -*-
"""
Created on Fri Nov 28 11:36:19 2014

@author: GiovanniPi
"""
import visa
from time import sleep
import HandlerIndirizzi as HI

class ErroriCMU(Exception):
    
    def __init__(self, message):
        self._message = message
    
    def __str_(self):
        return repr(self._message)
        
class CMU(object):
    bandaToAddress = {'GSM 850':('6', ""), 'GSM 900':('7', ""), 
                      'GSM 1800':('8', ""), 'GSM 1900':('9', ""), 
                      'UMTS 1':('11', '1'), 'UMTS 2':('11', '2'),
                      'UMTS 4':('11', '4'), 'UMTS 5':('11', '5'),
                      'UMTS 8':('11', '8')}

    step_Gsm = {0:[3, 75], 1:[2, 100], 2:[1.2,100], 3:[0.5,200], 4:[0.1, 300],
              5:[-0.2,300], 6:[-0.5,300], 7:[-1,300], 8:[-1,300], 9:[-1.1,300],
              10:[-1.2,300], 11:[-1.3,300], 12:[-1.8,300], 13:[-1.9,300],
              14:[-2.25,300]}

    def __init__(self):
        try:
            rm = visa.ResourceManager()
            self._cmu = rm.open_resource(HI.indirizzi.get('cmu'))
            op = self._cmu.query("*opt?")
            self._got_wcdma = False
            if 'K58' in op:
                self._cmu.write('syst:rem:addr:sec 10, "WCDMA19UEFDD_NSig"')
                self._cmu.write('syst:rem:addr:sec 11, "WCDMA19UEFDD_Sig"')
                self._got_wcdma = True
            self._cmu.write('syst:rem:addr:sec 2, "GSM850MS_NSig"')
            self._cmu.write('syst:rem:addr:sec 3, "GSM900MS_NSig"')
            self._cmu.write('syst:rem:addr:sec 4, "GSM1800MS_NSig"')
            self._cmu.write('syst:rem:addr:sec 5, "GSM1900MS_NSig"')
            self._cmu.write('syst:rem:addr:sec 6, "GSM850MS_Sig"')
            self._cmu.write('syst:rem:addr:sec 7, "GSM900MS_Sig"')
            self._cmu.write('syst:rem:addr:sec 8, "GSM1800MS_Sig"')
            self._cmu.write('syst:rem:addr:sec 9, "GSM1900MS_Sig"')
            self._gsm = None
            self._wcdma = None 
            #variabie booleana Vera se utilizzata tecnologia wcdma per link
            self.d_ON
        except visa.VisaIOError:
            raise ErroriCMU("CMU200 non collegato o spento")
            
    @property
    def cmuType(self):
        if self._got_wcdma:
            return 2 #indica tecnologia UMTS abilitata 
        return 1 #indica solo tecnologia GSM abilitata

    @property
    def rst(self):
        self._cmu.write("*rst")

    @property        
    def s_on(self):
        """Attiva la banda selezionata"""
        self._gsm.write("proc:sign:act son")
        
    @property
    def s_off(self):
        self._gsm.write("proc:sign:act soff")
    
    @property
    def d_ON(self):
        """Attiva visualizzazzione display quando in remoto"""
        self._cmu.write('trac:rem:mode:disp on')
    
    @property
    def off(self):
        """Va dato per fare la lettura di potenza e sensibilità"""
        self._cmu.write("syst:gtrm:comp off")
        
    @property
    def on(self):
        self._cmu.write("syst:gtrm:comp on")

    def setGsm(self, banda):
        """Assegna l'indirizzo secondario in base alla banda da misurare"""
        try:
            if self._gsm:
                self.s_off
            val, st = self.bandaToAddress.get(banda)
            rm = visa.ResourceManager()
            if 2 <= int(val) <= 9: 
                self._gsm = rm.open_resource("GPIB0::29::%s" % val)
                self._wcdma = False
            else:
                assert self._got_wcdma
                self._gsm = rm.open_resource("GPIB0::29::%s" % val)
                self._gsm.write('conf:netw:oban ob%s' % st)
                self._wcdma = True
            self.s_on
            self.off
        except AssertionError:
            raise ErroriCMU('Bande WCDMA non supportate')
    
    def write(self, comm):
        self._gsm.write(comm)
        
    def read(self):
        self._gsm.read

    def getInput(self):
        """Attenuazione su porta 2 in ingresso"""
        return float(self._gsm.ask("sens:corr:loss:inp2?"))
                
    def setInput(self, inp):
        try:
            assert inp <= 90 and inp >= -50 
            self._gsm.write("sens:corr:loss:inp2 %s" %inp)
        except AssertionError:
            raise ErroriCMU("Attenuazione fuori limite (-50:90)")

    attIn = property(getInput, setInput)

    def getOutput(self):
        """Attenuazione su prota 2 in uscita"""
        return float(self._gsm.ask("sens:corr:loss:outp2?"))
                
    def setOutput(self, out):
        try:
            assert out <= 90 and out >= -50 
            self._gsm.write("sens:corr:loss:outp2 %s" %out)    
        except AssertionError:
            raise ErroriCMU("Attenuazione fuori limite (-50:90)")
    
    attOut = property(getOutput, setOutput)
    
    def setBcch(self, ch):
        """Canale di controllo"""
        self._gsm.write("conf:bss:cch:chan %s;*opc?" % ch)
        
    def getBcch(self):
        return self._gsm.ask("conf:bss:cch:chan?")
        
    bcch = property(getBcch, setBcch)

    def setBcchLev(self, level):
        """Livello potenza canale di controllo"""
        self._gsm.write("conf:bss:cch:lev %s;*opc?" % level)
        
    def getBcchLev(self):
        return self._gsm.ask("conf:bss:cch:lev?")
    
    bcchLev = property(getBcchLev, setBcchLev)
    
    def setTch(self, channel):
        """Setta il canale di comunicazione"""
        if self._wcdma:
            self._gsm.write('conf:ues:chan %d' % channel)
        else:
            self._gsm.write("conf:bss:chan %d;*opc?" %channel)

    def getTch(self):
        if self._wcdma:
            return self._gsm.query('conf:ues:chan?')
        else:            
            return self._gsm.ask("conf:bss:chan?") 

    tch = property(getTch, setTch)

    def setTchL(self, level):
        """Setta livello canale di comunicazione lato dut"""
        if self._wcdma:
            self._gsm.write('conf:bss:opow %s' % level)
        else:
            self._gsm.write("conf:netw:pow %d;*opc?" %level)

    def getTchL(self):
        return self._gsm.ask("conf:netw:pow?") 

    tchLev = property(getTchL, setTchL)

    def setBTch(self, value):
        """Setta livello canale di comunicazione lato CMU"""
        self._gsm.write("conf:bss:tch:lev:utim -%s;*opc?" % value)

    def getBTch(self):
        return self._gsm.ask("conf:bss:tch:lev:utim?")

    btch = property(getBTch, setBTch)

    def power(self):
        """Legge la potenza e ritorna un valore definito da pos"""
        self._gsm.write("init:pow:max")
        sleep(0.1)
        try:
            if self._wcdma:
                return float(self._gsm.query("samp:pow:max?").split(',')[4])
            else:
                return float(self._gsm.query("read:pow?").split(',')[0])    
        except Exception:
            return False

    def readSens(self, frame):
        """Lettura ber, sincronizza timeout query con numero di frame"""
        try:
            if self.link:
                timeout = frame * 0.02 + 0.2
                self._gsm.write('init:rxq:ber')
                if self._wcdma:
                    ber = float(self._gsm.query('read:rxq:ber?', timeout).split(",")[0])
                else:
                    ber = float(self._gsm.query('read:rxq:ber?', timeout).split(",")[1])
                return ber
            else:
               return 'nan'
        except visa.VisaIOError:
            #significa che è avvenuto un timeout, ovvero durante la misura del
            #ber è caduto il link, bloccando l'acquisizione di frame.
            #il sistema va a legtgre prima che una risposta sia effettivamente
            #fornita, provocando la caduta del link
            return 'nan'

    def setLevBER(self, value):
        """Metodo per impostare la potenza del canale trasmissivo lato CMU
        quando si effettua la misura di BER"""
        if self._wcdma:
            self._gsm.write('conf:bss:opow %s' % value)
        else:
            self._gsm.write('conf:rxq:ber1:cont:lev:utim %s' % value)

    def setFrame(self, frames):
        """Settaggio numero di frame per misura"""
        if self._wcdma:
            self._gsm.write('conf:rxq:ber:cont:tbl %s' % frames)
        else:
            self._gsm.write('conf:rxq:ber1:cont ber, %s' % frames)

    def findBer(self, livello, frame):
        """Algoritmo di ricerca del ber ricorsivo"""
        self.setLevBER(livello)
        self.setFrame(frame)
        ber = self.readSens(frame)
        if str(ber).lower() == 'nan':
            return 'nan'
        if not self._wcdma:
            if 2.318 < ber < 2.562:
                self.setLevBER(-80)
                return livello
            if ber < 0.1:
                return self.findBer(livello - 5, 50)
            index = int(ber/0.5) if int(ber/0.5) < 14 else 14
            lev, frame = self.step_Gsm.get(index)
            return self.findBer(livello - lev, frame)
        else:
            if ber == 0.0:
                return self.findBer(livello - 1, 150)
            elif ber < 0.09:
                frame = frame * 2 if frame * 2 < 300 else 300
                return self.findBer(livello - 0.1, frame)
            else:
                self.setLevBER(-70)
                return livello

    @property
    def stopBer(self):
        """setta None come clausola di fine test, ovvero esegui sempre misura
        su tutti i frame, indipendentemente dal valore di Ber misurato.
        Step sempre settato su None"""
        if self._wcdma:
            self._gsm.write("conf:rxq:ber1:cont:rep sing,none,none")
        else:
            self._gsm.write("conf:rxq:ber1:cont:rep none, none")

    @property
    def link(self):
        if self._gsm.query("sens:sign:stat?").strip() == 'CEST':
            return True
        else: return False