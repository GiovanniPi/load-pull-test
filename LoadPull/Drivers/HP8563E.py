# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 09:47:33 2014

@author: GiovanniPi
"""
import visa
import HandlerIndirizzi as HI

class ErroriHP(Exception):
    
    def __init__(self, value):
        self._value = value
        
    def __str__(self):
        return repr(self._value)
        

class HP8563(object):
    
    validDet = ["ape","neg","pos","samp","rms","aver"]
    
    def __init__(self):
        try:
            rm = visa.ResourceManager()
            self._hp = rm.open_resource(HI.indirizzi.get('hp'))
        except visa.VisaIOError:
            raise ErroriHP("8563 non collegato o spento")
            
    @property
    def idn(self):
        """Ritorna stringa identificazione strumento"""
        return self._hp.query("*ID?;")
        
    @property
    def rst(self):
        """Resetta strumento"""
        self._hp.write("IP;")
        
    def setCenterFreq(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            assert valore.isalnum()
            self._hp.write("CF %s%s;" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriHP("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura separati da spazio
                            (max. 26.5 GHz)""")
    
    def readCenterFreq(self):
        return self._hp.query("CF?;")
        
    centerFreq = property(readCenterFreq, setCenterFreq, doc = """Imposta la 
        frequenza centrale, valore inserito è composto da
        valore numerico seguito da unita di misura (va lasciato spazio fra i 
        due)""")
    
    def setSpan(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            assert valore.isalnum()
            self._hp.write("SP %s%s;" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriHP("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura separati da spazio
                            """)

    def readSpan(self):
        return self._hp.query("SP?;")

    span = property(readSpan, setSpan, doc =  """Imposta lo span, il valore 
    inserito è composto da valore numerico seguito da unita di misura (va 
    lasciato spazio fra i due)""")
    
    def setDetector(self):
        try:
            self._hp.write("DET POS;")
        except AssertionError:
            raise ErroriHP("Detector non valido")
            
    def readDetector(self):
        return self._hp.query("DET?;")
    
    def setMarker(self):
        """Pone il marker sul picco massimo"""
        self._hp.write("MKPK HI;")
        
    def readMarker(self):
        """Legge il marker e ritorna l'ampiezza in dbm"""
        val = self._hp.query("MKA?;")
        return val.strip()

    @property
    def clear(self):
        self._hp.write('CLRW TRA;')
     
    @property
    def maxHold(self):
        self._hp.write("MXMH TRA;")

    def setRbw(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["mhz","khz","hz"]
            assert valore.isalnum()
            self._hp.write("RB %s%s;" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriHP("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura separati da spazio
                            """)

    def getRbw(self):
        return self._hp.query("RB?;")

    rbw = property(getRbw, setRbw, """Setta la RBW""")
        
    def setAttenuatore(self, value):
        try:        
            assert value >= 0
            self._hp.write("AT %dDB;" % value)
        except AssertionError:
            raise ErroriHP("Attenuazione deve essere un valore positivo")
            
    def getAttenuatore(self):
        return self._hp.query("AT?;")
        
    att = property(getAttenuatore, setAttenuatore, """Setta l'attenuatore""")