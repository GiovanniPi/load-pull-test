# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 09:47:33 2014

@author: GiovanniPi
"""
import visa
import HandlerIndirizzi as HI

class ErrorFSV(Exception):
    
    def __init__(self, value):
        self._value = value
        
    def __str__(self):
        return repr(self._value)
        

class FSV(object):
    
    validDet = ["ape","neg","pos","samp","rms","aver"]
    
    def __init__(self):
        try:
            rm = visa.ResourceManager()
            self._fsv = rm.open_resource(HI.indirizzi.get('fsv'))
            self.ON()
        except visa.VisaIOError:
            raise ErrorFSV("FSV non collegato o spento")

    @property
    def idn(self):
        """Return the identification of the instrument"""
        return self._fsv.query("*IDN?")
        
    def rst(self):
        self._fsv.write("*RST")

    def ON(self):
        self._fsv.write("system:display:update ON")

    @property
    def centerFreq(self):
        return self._fsv.query("freq:cent?")

    @centerFreq.setter
    def centerFreq(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            assert valore.isalnum()
            self._fsv.write("freq:cent %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErrorFSV("Insert a value  followed by it's unit of measurement")

    @property
    def span(self):
        return self._fsv.query("freq:span?")

    @span.setter
    def span(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            assert valore.isalnum()
            self._fsv.write("freq:span %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErrorFSV("Insert a value  followed by it's unit of measurement")

    @property
    def detector(self):
        return self._fsv.query("det?")

    @detector.setter
    def detector(self, det):
        try:
            assert det in self.validDet
            self._fsv.write("det %s" % det)
        except AssertionError:
            raise ErrorFSV("Insert a valid detector")

    def setMarker(self):
        """Set the marker on tha maximum peak detected"""
        self._fsv.write("calc:mark:max")

    @property
    def readMarker(self):
        """Return freq, dBm of the active marker as a tuple of strings"""
        x = self._fsv.query("calc:mark:x?")
        y = self._fsv.query("calc:mark:y?")
        return x, y

    @property
    def trace(self):
        return self._fsv.query("disp:trac:mode?")

    @trace.setter
    def trace(self, mod):
        try:
            assert mod.lower() in ["maxh","writ"]
            self._fsv.write("disp:trac:mode %s" % mod)
        except AssertionError:
            raise ErrorFSV("Invalid format requested [maxh, writ]")

    @property
    def rbw(self):
        return self._fsv.query("band?")

    @rbw.setter
    def rbw(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["mhz","khz","hz"]
            assert valore.isalnum()
            self._fsv.write("band %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErrorFSV("Insert a value  followed by it's unit of measurement")

    def setType(self):
        self._fsv.write("swe:type swe")
        
    @property
    def sweepType(self):
        return self._fsv.query("swe:type?")

    @property
    def attenuator(self):
        return self._fsv.query("inp:att?")

    @attenuator.setter
    def attenuator(self, value):
        """Return tha attenuation as a string"""
        try:
            assert value >= 0
            self._fsv.write("inp:att %ddB" % value)
        except AssertionError:
            raise ErrorFSV("Attenuation must be a positive number")
