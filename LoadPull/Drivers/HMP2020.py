# -*- coding: utf-8 -*-
"""
Created on Mon Dec 01 10:51:55 2014

@author: MicheleKo

Classe per la remotizzazione generatore DC Hameg HMP2020
"""

import visa
import HandlerIndirizzi as HI

class ErroriHMP2020(Exception):
    
    def __init__(self, message):
        self._message = message
        
    def __str__(self):
        return repr(self._message)

class HMP2020(object):
    
    def __init__(self):
        try:
            rm = visa.ResourceManager()
            self._hmp = rm.open_resource(HI.indirizzi.get('hmp'))
        except visa.VisaIOError:
            raise ErroriHMP2020("HMP2020 non collegato o spento")
    
    @property
    def idn(self):
        """Identifica lo strumento."""
        return self._hmp.query("*IDN?")

    @property
    def rst(self):
        """Resetta lo strumento."""
        self._hmp.write("*rst")
        
    def getChannel(self):
        """
        Restituisce il canale selezionato selezionato.
        """
        return self._hmp.ask("inst?")
    
    def setChannel(self, value):
        """
        Value è 1 per la porta uno e 2 per la porta 2.
        """
        try:
            assert value == 1 or value ==2
            self._hmp.write("inst out%s" % value)
        except (AssertionError):
            raise ErroriHMP2020("""Inserisci la stringa 1 per la porta uno""" 
            """ e 2 per la porta due.""")

    channel = property(getChannel, setChannel, doc = """Seleziona il canale"""
                          """su cui settate o leggere i patametri di misura.""")

    def getOutputState(self):
        """
        Restituisce lo stato del canale (0 non attivo, 1 attivo).
        """
        return int(self._hmp.ask("outp?").strip())
    
    def setOutputState(self, value):
        """Value è uno 0 se la porta nn è attiva 1 se la porta è attiva."""
        try:
            assert value == 0 or value ==1
            self._hmp.write("outp %s" % value)
        except (AssertionError):
            raise ErroriHMP2020("""Inserisci una strigna 1 per la porta uno""" 
            """ e 2 per la porta due""")

    state = property(getOutputState, setOutputState, doc = """Seleziona la porta"""
                          """su cui settate o leggere i parametri di misura""")
                          
    def getOutputGeneral(self):
        """
        attiva il tasto OUTPUT
        """
        return self._hmp.ask("outp:gen?")
    
    def setOutputGeneral(self, value):
        """Attiva o disattiva il general OUTPUT"""
        try:
            assert value == 0 or value ==1
            self._hmp.write("outp:gen %s" % value)
        except (AssertionError):
            raise ErroriHMP2020("""Inserisci una strigna 1 per la porta uno""" 
            """ e 2 per la porta due""")

    output_general = property(getOutputGeneral, setOutputGeneral, 
                              doc = """attiva il general output""")
                        
    def getSetCurrent(self):
        """
        restituisce la corrente a cui è settata la porta selezionata
        """
        return self._hmp.query("curr?")
    
    def setSetCurrent(self, value):
        """Inserisci un valore da 0 a 10 per la porta 1 e 0 e 5 per la 
        porta 2"""
        try:
            assert value >= 0 and value <=10
            self._hmp.write("curr %s" % value)
        except (AssertionError):
            raise ErroriHMP2020("""Inserisci un valore da 0 a 10 per la porta 1"""
                                """e 0 e 5 per la porta 2""")
 
    #settaggio frequenza inzio
    amp = property(getSetCurrent, setSetCurrent, doc = """Imposta o leggi il"""
                          """valore della corrente a cui è settata la porta""")

    def getSetVoltage(self):
        """
        restituisce la corrente massima settata alla porta selezionata
        """
        return self._hmp.query("volt?")
    
    def setSetVoltage(self, value):
        """Inserisci un valore da 0 a 32 per la porta 1 e 0 e 32 per la porta 2"""
        try:
            assert value >= 0 and value <=32
            self._hmp.write("volt %s" % value)
        except (AssertionError):
            raise ErroriHMP2020("""Inserisci un valore da 0 a 32 per la porta 1"""
                                """e 0 e 32 per la porta 2""")

    volt = property(getSetVoltage, setSetVoltage, doc = """Imposta o leggi il"""
                          """vaolore della tensione a cui è settata la porta""")
                          
    @property                 
    def current(self):
        """
        restituisce la corrente letta dalla porta attiva
        """
        return float(self._hmp.query("meas:curr?"))
                          
    @property
    def voltage(self):
        """
        restituisce la tensione letta dalla porta attiva
        """
        return float(self._hmp.query("meas?"))
