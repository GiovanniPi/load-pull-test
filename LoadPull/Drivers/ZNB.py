# -*- coding: utf-8 -*-
"""
Created on Mon Nov 10 09:53:22 2014

@author: GiovanniPi
"""

import visa

class ErroriZNB(Exception):
    
    def __init__(self, message):
        self._message = message
        
    def __str__(self):
        return repr(self._message)

class ZNB(object):
    
    convertTrace = {1:"Trc1",2:"Trc2",3:"Trc3",4:"Trc4"}
    convertResult = {1:"s11",2:"s21",3:"s12",4:"s22"}
    validMode = ["mlin","mlog","phas","uph","pol","smit","ism","gdel","real",
                 "imag","swr","comp","magn"]
    
    def __init__(self):
        try:
            rm = visa.ResourceManager()
            self._znb = rm.open_resource("GPIB::22")
        except visa.VisaIOError:
            raise ErroriZNB("Instrument not connected")
    
    @property
    def idn(self):
        """identifica lo strumento"""
        return self._znb.query("*IDN?")

    @property
    def rst(self):
        """resetta lo strumento"""
        self._znb.write("*rst")
        
    @property    
    def loc(self):
        """Manda in locale"""
        self._znb.write("@LOC")
        
    def getStart(self):
        return self._znb.query("freq:start?")
    
    def setStart(self, value):
        """Value è un valore numerico seguito da unità di misura con spazio"""
        try:
            valore, unita = value.split(" ")
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            self._znb.write("freq:start %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriZNB("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura (es. 150 MHz)""")
    
    #settaggio frequenza inzio
    start = property(getStart, setStart, doc = """Setta la frequenza di inizio,
                                                va inserita con unità di misura""")

    def getStop(self):
        return self._znb.query("freq:stop?")

    def setStop(self, value):
        """Value è un valore numerico seguito da unità di misura con spazio"""
        try:
            valore, unita = value.split(" ")
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            self._znb.write("freq:stop %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriZNB("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura (max. 20 GHz)""")

    #settaggi frequenza fine scansione
    stop = property(getStop, setStop, doc = """Setta la frequenza di fine,
                                                va inserita con unità di misura""")

    def setTrace(self, trace_n, mode = 'smit'):
        try:
            trace = self.convertTrace.get(trace_n)
            result = self.convertResult.get(trace_n)
            assert trace is not None
            assert mode.lower() in self.validMode
            if trace == 'Trc1':
                self._znb.write("calc1:par:meas '%s', '%s'" % (trace, result))
            else:
                self._znb.write("calc1:par:sdef '%s', '%s'" % (trace, result))
                self._znb.write("disp:trac%d:feed '%s'" % (trace_n, trace))
            self._znb.write("calc1:form %s" % mode)
        except AssertionError:
            pass

    def readTrace(self, trace):
        try:
            trace = self.convertTrace.get(trace)
            assert trace is not None
            response = self._znb.query("calc1:data:trace? '%s', sdata" % trace)
            answer = response.split(",")
            re = []
            im = []
            for index, value in enumerate(answer):
                if index%2 == 0:
                    re.append(float(value))
                else:
                    im.append(float(value))
            return re, im                    
                    
        except AssertionError:
            raise ErroriZNB("Inserisci un valore tra 1-4")

    def getPoints(self):
        return self._znb.query("sweep:points?")

    def setPoints(self, points):
        try:
            points = int(points)
            assert points > 0 and points < 1002
            self._znb.write("sweep:points %d" % points)
        except (ValueError, AssertionError):
            raise ErroriZNB("Inserire un intero compreso tra 1 e 1001")

    @property
    def listaFrequenze(self):
        """Ritorna un vettore che contiene i valori dell'asse ascisse"""
        response = self._znb.query("calc1:data:stim?")
        return map(lambda x:float(x) / 1000000000, response.split(","))

    points = property(getPoints, setPoints)

    def getSource(self):
        return self._znb.query("sour:pow?")
        
    def setSource(self, value):
        self._znb.write("sour:pow %s" % value)
    
    source = property(getSource, setSource)
