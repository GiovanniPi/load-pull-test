# -*- coding: utf-8 -*-
"""
Created on Tue Dec 16 16:37:02 2014

@author: GiovanniPi
"""

import pyqtgraph as pg


class SmithChart(pg.PlotWidget):

    diz_plot = {'Potenza':[(255, 0, 0),'x'],
                'Sensibilita':[(255, 255, 255), 'o'],
                'Consumi':[(0, 0, 255), '+'],
                'Armoniche':[(0, 255, 0), 's'],
                'Radiazione':[(255, 0, 255), 't'],
                'Potenza FSV':[(255, 255, 0), 'd']}
    
    def __init__(self, parent = None):
        super(SmithChart, self).__init__(parent, enabledMenu = False)
        self._last_point = None
        self.dirty = False
        x = [-1,1]
        y = [0,0]
        self.setAspectLocked()
        self.setXRange(-1,1)
        self.setYRange(-1,1)
        self.plot(x,y, pen = pg.mkPen('g', width = 0.2))
        rline = [0.2, 0.5, 1.0, 2.0, 5.0]
        xline = [0.2, 0.5, 1, 2, 5]
        circle1 = pg.QtGui.QGraphicsEllipseItem(1, -1, -2, 2)
        circle1.setPen(pg.mkPen('w', width=0))
        circle1.setFlag(circle1.ItemClipsChildrenToShape)
        self.addItem(circle1)
        pathItem = pg.QtGui.QGraphicsPathItem()
        path = pg.QtGui.QPainterPath()
        path.moveTo(1, 0)
        
        for r in rline:
            raggio = 1./(1+r)
            path.addEllipse(1, -raggio, -raggio*2, raggio*2)
        
        for x in xline:
            path.arcTo(x + 1, 0, -x*2, x*2, 90, -180)
            path.arcTo(x + 1, 0, -x*2, -x*2, 270, 180)
            
        pathItem.setPath(path)
        pathItem.setPen(pg.mkPen('g', width = 0.2))
        pathItem.setParentItem(circle1)

        self._scatter = pg.ScatterPlotItem(size = 4)
        self.addItem(self._scatter)
        self.setFixedSize(700, 700)
        
    def addPoint(self, coord, name, value):
        """
        :param coord: coordinates of the point as complex value
        :param value: magnitude of the point
        :param name: name of the parameter"""
        x = coord.real
        y = coord.imag
        color, symbol = self.diz_plot.get(name)
        self._scatter.addPoints([x], [y], brush = pg.mkBrush(color = color), symbol = symbol)
        self.dirty = True
        return name + value

    def clearPlot(self):
        if self.dirty:
            self._scatter.clear()
            self.dirty = False
