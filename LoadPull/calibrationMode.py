# -*- coding: utf-8 -*-
"""
Created on Fri Nov 07 13:58:20 2014

@author: GiovanniPi
"""

#import CLasseZVB
import logging
from datetime import datetime
from os import path, makedirs
from time import sleep

from PyQt4 import QtGui, QtCore

from LoadPull.Drivers.ZNB import ZNB
from TouchstoneS2P import TouchstoneS2P


class Calibrazione(QtCore.QObject):
    finished = QtCore.pyqtSignal()
    update_counter = QtCore.pyqtSignal([float])
    path = "C:\Users\emclab1\Documents\load-pull-test\LoadPull\Calibrazioni"
    
    def __init__(self, motore, rele):
        super(Calibrazione, self).__init__()
        self._swr = ""
        self._filtro = ""
        self._porta = ""
        self._s2p = TouchstoneS2P()
        self._znb = ZNB()
        self._rele = rele
        self._motori = motore
        self._logger = logging.getLogger("Calibration")
        self._logger.setLevel(logging.DEBUG)
        self._freq_list = []
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self._logger.addHandler(ch)
        self.settaZNB()
        
    def routineCalibrazione(self):
        self._logger.info("Start calibration")
        self._logger.debug("Set load: %s" % self._swr)
        self._rele.setSwr(self._swr)
        sleep(0.5)
        pos = 0
        counter = 1.0
        while pos < 71000:
            self._logger.debug("Moving to %d" % pos)
            if pos is not 0:
                flag_busy = self._motori.goTo(str(pos))
                while flag_busy:
                    flag_busy = self.checkMotori()
                    sleep(0.5)
            self.update_counter.emit(counter)
            counter += 1
            sleep(0.1)
            self._logger.info("In position")
            self.misura(pos, self._swr, self._filtro)
            pos += 246
        flag_busy = self._motori.reset()
        while flag_busy:
            flag_busy = self.checkMotori()
            sleep(0.5)
        self._logger.info("Reset completato")
        self.finished.emit()

    def checkMotori(self):
        response = self._motori.read
        if response:
            clean = response.strip()
            print clean
            if clean == "D":
                return False
        return True
        
    def misura(self, pos, key, filtro):
        self._logger.info("Reading data")
        re11 = []
        im11 = []
        re21 = []
        im21 = []
        re22 = []
        im22 = []
        re12 = []
        im12 = []
        re11, im11 = self._znb.readTrace(1)
        sleep(0.3)
        re21, im21 = self._znb.readTrace(2)
        sleep(0.3)
        re22, im22 = self._znb.readTrace(4)
        sleep(0.3)
        re12, im12 = self._znb.readTrace(3)
        sleep(0.3)
        self._logger.info("saving data")
        nome = "_".join([str(pos), key, filtro])
        nome = "\\".join(['Calibrazioni',self._porta, self._filtro, self._swr, nome])
        self._s2p.name = nome
        self._s2p.comment = str(datetime.now())
        self._s2p.write(self._freq_list, re11, im11, re21, im21, re12, im12,
                        re22, im22)
        sleep(0.5)
     
    def settaZNB(self):
        self._znb.points = 2511
        self._znb.start = "450 MHz"
        self._znb.stop = "13 GHz"
        self._logger.info("Settagio ZNB")
        self._znb.source = "0"
        for i in xrange(1,5):
            self._znb.setTrace(i)
            sleep(0.1)
        self._freq_list = self._znb.listaFrequenze
        
    def settaParametri(self, swr, filtro, porta):
        self._swr = swr
        self._rele.setFiltro(filtro)
        sleep(0.2)
        self._filtro = filtro[-4:]
        self._porta = porta
        dir_s = "\\".join([self.path, porta, self._filtro, swr])
        if not path.exists(dir_s):
            makedirs(dir_s)
  

class CalibrazioneGUI(QtGui.QDialog):
    
    def __init__(self, motore, rele, parent = None):
        super(CalibrazioneGUI, self).__init__(parent)
        
        self.obj_thread = QtCore.QThread()
        self.thread_cal = Calibrazione(motore, rele)
        self.thread_cal.moveToThread(self.obj_thread)
        self.thread_cal.update_counter.connect(self.updateProgress)
        self.thread_cal.finished.connect(self.obj_thread.quit)
        self.obj_thread.started.connect(self.thread_cal.routineCalibrazione)
        self.obj_thread.finished.connect(self.onFinished)

        self.start_btn = QtGui.QPushButton("Start")
        self.start_btn.clicked.connect(self.start)

        self.combo_swr = QtGui.QComboBox()
        self.combo_swr.addItems(["1","1.5","2","3","4","6","10","Open"])
        self.combo_swr.setMaximumWidth(80)
        self.combo_fil = QtGui.QComboBox()
        self.combo_fil.addItems(["High-pass 3400","High-Pass 1200"])
        self.combo_fil.setMaximumWidth(80)
        self.combo_por = QtGui.QComboBox()
        self.combo_por.addItems(["PortA", "PortB"])
        self.combo_por.setMaximumWidth(80)
        
        self.progress_bar = QtGui.QProgressBar(self)
        self.progress_bar.setRange(0, 100)
        self.progress_bar.setFixedWidth(150)
        
        hor_box = QtGui.QHBoxLayout()
        hor_box.addWidget(self.start_btn)
        grid = QtGui.QGridLayout()
        grid.addWidget(self.combo_swr, 0, 1, 1, 1)
        grid.addWidget(self.combo_fil, 1, 1, 1, 1)
        grid.addWidget(self.combo_por, 2, 1, 1, 1)
        grid.addLayout(hor_box, 3, 1, 1, 1)
        grid.addWidget(self.progress_bar, 4, 0, 1, 3)

        self.setLayout(grid)
        self.setWindowTitle("Calibrazione")

    def start(self):
        self.start_btn.setEnabled(False)
        self._counter = 0
        self.thread_cal.settaParametri(str(self.combo_swr.currentText()),
                                       str(self.combo_fil.currentText()),
                                       str(self.combo_por.currentText()))
        self.obj_thread.start()

    def updateProgress(self, i):
        self.progress_bar.setValue(int(i/284*100))
        
    def onFinished(self):
        self.start_btn.setEnabled(True)
