# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 12:05:25 2014

@author: GiovanniPi

Modulo per le misure
Tutte le misure sono classi di tipo QObject, possono essere eseguite in thread 
separati usando la funzione moveToThread. La struttura delle misure è sempre la 
stessa, ovvero:
-controlloFOlder: crea la cartella e salva il percorso
-start Thread: dove vengono recuperati eventuali parametri comnui, specialmente
             nella misura di armoniche
-leggi: Funzione di lettura dello strumento, e in certi casi anche settaggio di 
        parametri (ad esempio frequenza centrale FSV...)
"""
from PyQt4 import QtCore, QtGui
from FSV import FSV, ErroriFSV
from CMU import CMU, ErroriCMU
from HP8563E import HP8563, ErroriHP
from HMP2020 import HMP2020, ErroriHMP2020
from time import sleep
from os import chdir, path, makedirs
from itertools import islice
import linecache
from UtilsSP import Matrici
import numpy as np

def salvaSetupTest(directory, wrapper):
    name = directory + '\\setup.txt'
    with open(name, 'a+') as f:
        a = QtCore.QDateTime().currentDateTime()
        f.write(str(a.toString(QtCore.Qt.ISODate)) + '\n')
        testo = '''CWG: %s\nPorta A: %s\nPorta B: %s\nConnettore C: %s\n''' \
                %(wrapper.transCwg, wrapper.transA, wrapper.transB, wrapper.transC)
        f.write(testo)

class ErroriMisura(Exception):

    def __init__(self, message):
        self._mess = message

    def __str__(self):
        return repr(self._mess)

class Armoniche(QtCore.QObject):
    """Classe per la lettura delle armoniche, emette il segnale armonica_done
    quando misura completata. \n
    Il parametro s11 è riferito sul piano di misura dell'FSV.\n
    Funzione beforeStart va eseguita all'inzio della misura, ogni volta che
    viene cambiato un parametro di misura (SWR, Filtro, Banda...)\n
    """
    armonica_done = QtCore.pyqtSignal([list])

    def __init__(self, wrapper):
        try:
            super(Armoniche, self).__init__()
            self._fsv = FSV()
            self._fsv.rst
            self._wrapper = wrapper
            self._compensa = Compensazione(wrapper, 'PortA')
            self._s11 = None
            self.limite = None
        except ErroriFSV as e:
            raise ErroriMisura(str(e))

    def leggiArmonica(self, work_freq, index, work_dir, vect):
        """Metodo ricorsivo per la misura dell'armonica,
        In parole povere setta lo spettro e successivamente recupera i
        parametri dai transducer per la frequenza d'armonica.
        Successivamente aspetta 4 secondi per
        permettere allo spettro di stabilizzare la traccia. Il marker viene
        settato sul picco e letto. Vengono salvati in un file csv nominato
        con il valore di frequenza, in cui la riga è composta dal valore s11
        s12 e db armonica. La condizione di fine è vera se ho fatto almeno 3
        armoniche (caso bande basse) o supero i 5GHz (caso bande alte)"""
        if work_freq > self.limite:
            self._wrapper.is_ok = True
            stringa = ' '.join(vect)
            self.armonica_done.emit([True, 'Armoniche', self._s11, stringa])
            return
        self._fsv.trace = "writ"
        self._fsv.centerFreq = str(work_freq) + " Hz"
        self._fsv.trace = "maxh"
        s = self._compensa.getS(work_freq)
        #aspetta per stabilizzare la traccia
        sleep(4)
        self._fsv.setMarker()
        sleep(0.2)
        x, y = self._fsv.readMarker()
        val = -20*np.log10(np.absolute(complex(s[0, 1]))) + float(y)
        name = work_dir + "\\" + str(work_freq) + '.csv'
        with open(name, 'a+') as f:
            line = ",".join([str(self._s11), str(val), '\n'])
            f.write(line)
            vect.append('%s[dBm];' % str(val))
        self.leggiArmonica(work_freq + self._wrapper.frequency, index + 1,
                          work_dir, vect)

    def checkFolder(self):
        work_dir = "\\".join([self._wrapper.saveDir, self._wrapper.band,
                              "Armoniche", self._wrapper.swr])
        if not path.exists(work_dir):
            makedirs(work_dir)
        self._work_dir = work_dir

    def start(self):
        """Funzione di inzio misura, associata all'inzio thread. Legge il
        parametro s11 comune alle armoniche e lancia la funzione di lettura"""
        #parametro s._11 relativo alla frequenza portante è condiviso con
        #tutte le armoniche, è quello che da pozione su carta
        work_freq = self._wrapper.frequency * 2
        self._s11 = self._compensa.getS(self._wrapper.frequency)[0, 0]
        self.leggiArmonica(work_freq, 0, self._work_dir, [': '])

    def beforeStart(self):
        self._fsv.att = 0
        self._fsv.rbw = "1 MHz"
        self._fsv.setType()
        if 'GSM' in self._wrapper.band:
            self._fsv.detector = 'pos'
            self._fsv.span = "5 MHz"
            self.limite = 4000000000
        else:
            self._fsv.detector = 'rms'
            self._fsv.span = "10 MHz"
            self.limite = 12750000000


class PotFSV(QtCore.QObject):

    potenza_fsv_done = QtCore.pyqtSignal([list])

    def __init__(self, wrapper):
        try:
            super(PotFSV, self).__init__()
            self._fsv = FSV()
            self._wrapper = wrapper
            self._compensa = Compensazione(wrapper, 'PortA')
            self._fsv.rst
        except ErroriFSV as e:
            raise ErroriMisura(str(e))

    def checkFolder(self):
        work_dir = "\\".join([self._wrapper.saveDir, self._wrapper.band,
                              "Potenza FSV", self._wrapper.swr])
        if not path.exists(work_dir):
            makedirs(work_dir)
        salvaSetupTest(work_dir, self._wrapper)
        self._work_dir = work_dir
        self.nome_file = self._work_dir + "\\" + str(self._wrapper.frequency) + '.csv'
        open(self.nome_file, 'w+').close()

    def leggiPotenza(self):
        self._fsv.trace = "writ"
        work_freq = str(self._wrapper.frequency)
        self._fsv.centerFreq = work_freq + " Hz"
        s = self._compensa.getS(self._wrapper.frequency)
        self._fsv.trace = "maxh"
        sleep(4)
        self._fsv.setMarker()
        sleep(0.2)
        x, y = self._fsv.readMarker()
        val = -20*np.log10(np.absolute(complex(s[0, 1]))) + float(y)
#        name = self._work_dir + "\\" + work_freq + '.csv'
        with open(self.nome_file, 'a+') as f:
            line = ",".join([str(s[0, 0]), str(val), '\n'])
            f.write(line)
        self.potenza_fsv_done.emit([True, 'Potenza FSV', s[0, 0], ': %d[dBm]' % val])

    def beforeStart(self):
        self._fsv.att = 0
        self._fsv.rbw = "1 MHz"
        self._fsv.setType()
        if 'GSM' in self._wrapper.band:
            self._fsv.detector = 'pos'
            self._fsv.span = "5 MHz"
        else:
            self._fsv.detector = 'rms'
            self._fsv.span = "10 MHz"


class Consumi(QtCore.QObject):
    """Classe lettura consumi modulo, per ora lettura è la media su 30 campioni
    letti ogni decimo di secondo, per corrente e tensione; emette il segnale
    consumi_done a misura finita\n ATTENZIONE: il DUT va collegato a porta 1
    """
    #list va fatta nel seguente: val complesso coordinate punto, nome parametro,
    # valore misurato (colore punto)
    consumi_done = QtCore.pyqtSignal([list])
    #il piano di riferimento per la posizione è la porta b

    def __init__(self, wrapper = None):
        try:
            super(Consumi, self).__init__()
            self._wrapper = wrapper
            self._hmp = HMP2020()
            self._hmp.channel = 1
            self._compensa = Compensazione(wrapper, 'PortB')
        except ErroriHMP2020 as e:
            raise ErroriMisura(str(e))

    def checkFolder(self):
        work_dir = "\\".join([self._wrapper.saveDir, self._wrapper.band,
                              "Consumi", self._wrapper.swr])
        if not path.exists(work_dir):
            makedirs(work_dir)
        salvaSetupTest(work_dir, self._wrapper)
        self._work_dir = work_dir
        self.nome_file = self._work_dir + "\\" + str(self._wrapper.frequency) + '.csv'
        open(self.nome_file, 'w+').close()

    def start(self):
        """Inizio thread, impost alcuni parametri di funzione, non
        completamente necessaria, la tengo per rendere uniforme la struttura
        di tutte le misure"""
        self.leggi(30, self._work_dir, self._wrapper.frequency)

    def leggi(self, n_sample, w_dir, freq):
        v, a = [], []
        for i in range(n_sample):
            if self._hmp.state:
                v.append(self._hmp.voltage)
                a.append(self._hmp.current)
                sleep(0.1)
            else:
                self.consumi_done.emit([False, 'Consumi', "L'EUT e' spento accenderlo e premere ok, "])
                return
        v_media = sum(v) / len(v)
        a_media = sum(a) / len(a)
        s = self._compensa.getS(freq)[0, 0]
        with open(self.nome_file, 'a+') as f:
            line = ",".join([str(s), str(v_media), str(a_media),'\n'])
            f.write(line)
        self.consumi_done.emit([True, 'Consumi', s,
                               ': %s[V], %s[A]' % (str(v_media), str(a_media))])


class Pot(QtCore.QObject):
    """La classe Pot contiene le routine per la misura di potenza
    quando l'applicazione sta facendo una chiamata di tipo voce.
    Inoltre funziona anche da wrapper per il driver della CMU e fornisce
    alcune funzioni base, come checkLink e settaggio banda gsm."""
    potenza_done = QtCore.pyqtSignal([list])

    def __init__(self, wrapper):
        try:
            super(Pot, self).__init__()
            self._wrapper = wrapper
            self._cmu = CMU()
            self._compensa = Compensazione(wrapper, 'PortB')
            self._previous_sens = None
        except ErroriCMU as e:
            raise ErroriMisura(str(e))

    def checkFolderPot(self):
        work_dir = "\\".join([self._wrapper.saveDir, self._wrapper.band,
                              "Potenza", self._wrapper.swr])
        if not path.exists(work_dir):
            makedirs(work_dir)
        salvaSetupTest(work_dir, self._wrapper)
        self._work_dir_pot = work_dir
        self.nome_file = work_dir + "\\" + str(self._wrapper.frequency) + '.csv'
        open(self.nome_file, 'w+').close()

    def startPot(self):
        #servono due parametri questa volta, ovvero s11 e s12
        s = self._compensa.getS(self._wrapper.frequency)
        self.leggiPot(self._work_dir_pot, s)

    def leggiPot(self, work_dir, s):
        """Metodo per il calcolo della potenza in uscita dal modulo. La riga di
        file è composta da s11, valore di potenza (ottenuto sommando la potenza
        letta all'attenuazione reale ottenuto dall'embedding e la compensazione
        della CMU)"""
        #inizia con uno sleep in modo da lasciare un minimo di assestamento
        #nella nuova posizione
        sleep(1)
        pot = self._cmu.power()
        if not pot:
            self.potenza_done.emit([False, 'Potenza', 'Errore durante la misura di potenza, controllare il link e premere ok, '])

        att = self._cmu.attOut
        #potenza fornita  dipende da pot_letta, attenuazione impostata CMU
        # e attenuazione reale calcolata da transducer
        val = -20*np.log10(np.absolute(s[0, 1])) - att + pot
        if val > 0: #umts ogni tanto ha bisogno di tempo prima di dichiarare il link
        #caduto, controllando se valore negativo evito giri a vuoto
            with open(self.nome_file, 'a+') as f:
                line = ",".join([str(s[0, 0]), str(val), '\n'])
                f.write(line)
                self.potenza_done.emit([True, 'Potenza', s[0, 0], ': %s[dBm]' % str(val)])
        else:
            self.potenza_done.emit([False, 'Potenza', 'Errore durante la misura di potenza, controllare il link e premere ok, '])

    def settaBanda(self, banda):
        try:
            self._cmu.setGsm(banda)
        except ErroriCMU:
            raise ErroriMisura('Banda non supportata dalla CMU')

    def checkLink(self):
        """Esegue il controllo del link; idealmente va prima delle misure,
        dopo il riposizionamento della guida"""
        if not self._cmu.link:
            raise ErroriMisura("Non in link")

    @property
    def cmuType(self):
        """Ritorna il tipo di cmu, 2 abilitata anche umts, 1 solo gsm"""
        return self._cmu.cmuType

    @property
    def cmu(self):
        """ritorna l'istanza cmu. per poterla passare direttamente alla classe
        di sensibilità"""
        return self._cmu


class Sens(QtCore.QObject):
    """Per poter usare un thread diverso, va creato un'oggetto diverso, quindi
    questa classe fa solo la misura di sensibilita'. Non inizializza la CMU,
    che viene passata tramite la classe Pot
    """
    sens_done = QtCore.pyqtSignal([list])
    deltaRX = {'GSM 850':45000000.0, 'GSM 900': 45000000.0,
               'GSM 1900':80000000.0, 'GSM 1800':95000000.0,
               'UMTS 1':190000000.0, 'UMTS 2':80000000.0,
               'UMTS 4':400000000.0, 'UMTS 5':45000000.0,
               'UMTS 8':45000000.0}

    def __init__(self, wrapper, cmu):
        super(Sens, self).__init__()
        self._cmu = cmu
        self._wrapper = wrapper
        self._compensa = Compensazione(wrapper, 'PortB')
        self._previous_sens = None
        self._parent_widget = QtGui.QWidget()

    def checkFolderSens(self):
        work_dir = "\\".join([self._wrapper.saveDir, self._wrapper.band,
                              "Sensibilita", self._wrapper.swr])
        if not path.exists(work_dir):
            makedirs(work_dir)
        salvaSetupTest(work_dir, self._wrapper)
        self._work_dir_sens = work_dir
        self._previous_sens = 0
        freq_rx = self._wrapper.frequency + self.deltaRX.get(self._wrapper.band)
        self.nome_file = work_dir + "\\" + str(int(freq_rx)) + '.csv'
        open(self.nome_file, 'w+').close()

    def startSens(self):
        #servono due parametri questa volta, ovvero s11 e s12
        freq_rx = self._wrapper.frequency + self.deltaRX.get(self._wrapper.band)
        s = self._compensa.getS(freq_rx)
        self.leggiSens(self._work_dir_sens, s, freq_rx)

    def leggiSens(self, work_dir, s, freq_rx):
        if self._previous_sens != 0:
            self._previous_sens = self._cmu.findBer(self._previous_sens + 3, 200)
        else:
            self._previous_sens = self._cmu.findBer(-95, 50)
        if self._previous_sens == 'nan':
            self._previous_sens = -95
            self.sens_done.emit([False, 'Sensibilita', "Errore durante la misura di sensibilita', controllare il link e premere ok"])
#        name = work_dir + "\\" + str(int(freq_rx)) + '.csv'
        att = self._cmu.attIn
        val = -(-20*np.log10(np.absolute(s[0, 1])) - att) + self._previous_sens
        with open(self.nome_file, 'a+') as f:
            line = ",".join([str(s[0, 0]), str(val), '\n'])
            f.write(line)
            self.sens_done.emit([True, 'Sensibilita', s[0, 0], ': %s[dBm]' %str(val)])

    def stopBer(self):
        self._cmu.stopBer


class Radiazione(QtCore.QObject):
    radiazione_done = QtCore.pyqtSignal([list])

    def __init__(self, wrapper):
        super(Radiazione, self).__init__()
        self._wrapper = wrapper
        self._hp = HP8563()
        self._compensa = Compensazione(wrapper, 'PortB')

    def checkFolder(self):
        work_dir = "\\".join([self._wrapper.saveDir, self._wrapper.band,
                              "Radiazione", self._wrapper.swr])
        if not path.exists(work_dir):
            makedirs(work_dir)
        salvaSetupTest(work_dir, self._wrapper)
        self._work_dir = work_dir
        self.nome_file = work_dir + "\\" + str(self._wrapper.frequency) + '.csv'
        open(self.nome_file, 'w+').close()

    def beforeStart(self):
        if 'GSM' in self._wrapper.band:
            self._hp.span = "1 MHz"
        else:
            self._hp.span = "10 MHz"
        self._hp.att = 0
        self._hp.rbw = "100 KHz"
        self._hp.setDetector()

    def leggiRadiazione(self, work_freq, index, work_dir, vect):
        """Metodo ricorsivo per la misura dell'armonica,
        In parole povere setta lo spettro e successivamente recupera i
        parametri dai transducer per la frequenza d'armonica.
        Successivamente aspetta 4 secondi per
        permettere allo spettro di stabilizzare la traccia. Il marker viene
        settato sul picco e letto. Vengono salvati in un file csv nominato
        con il valore di frequenza, in cui la riga è composta dal valore s11
        s12 e db armonica. La condizione di fine è vera se ho fatto almeno 3
        armoniche (caso bande basse) o supero i 5GHz (caso bande alte)"""
        if work_freq > 5000000000 or index > 2:
            stringa = ' '.join(vect)
            self.radiazione_done.emit([True, 'Radiazione', self._s11, stringa])
            return
        self._hp.clear
        self._hp.centerFreq = str(work_freq) + " Hz"
        self._hp.maxHold
        #aspetta per stabilizzare la traccia
        sleep(4)
        self._hp.setMarker()
        sleep(0.2)
        val = self._hp.readMarker()
        with open(self.nome_file, 'a+') as f:
            line = ",".join([str(self._s11), str(val), '\n'])
            f.write(line)
            vect.append('%s[dBm];' % str(val))
        self.leggiRadiazione(work_freq + self._wrapper.frequency, index + 1,
                          work_dir, vect)

    def start(self):
        """Funzione di inzio misura, associata all'inzio thread. Legge il
        parametro s11 comune alle armoniche e lancia la funzione di lettura"""
        #parametro s._11 relativo alla frequenza portante è condiviso con
        #tutte le armoniche, è quello che da pozione su carta
        work_freq = self._wrapper.frequency * 2
        self._s11 = self._compensa.getS(self._wrapper.frequency)[0, 0]
        self.leggiRadiazione(work_freq, 0, self._work_dir, [': '])


class Compensazione(object):
    """Classe per l'embedding dei transducer, va dichiarata passando il file di
    wrapper e la porta relatico allo strumento che stiamo analzzando.\n
    La funzione principale è getS(freq)->matrice S embedding\n
    -findCavo serve a leggere i parametri del cavo, vengono recuperati in base
    al parametro porta. I file letti con il metodo linecache, poichè vengono
    fatti molti accessi sullo stesso file
    -find() serve a leggere i file di calibraizone, utilizza metodo islice,
    i cui tempi di accesso sono indipendenti dal numero di riga che va letta"""

    comp_folder = "C:\Users\emclab1\Documents\load-pull-test\LoadPull\Calibrazioni"
    skip_line = "#","!"

    def __init__(self, wrapper, porta):
        """Esegue accesso ai file di calibrazione; esegue embedding per trovare
        la posizione sulla carta di smith di un determinato carico"""
        self._wrapper = wrapper
        self._porta = porta
        self._matrici = Matrici()

    def find(self, freq):
        """Legge i parametri dai file di calibrazione del tuner, usando i
        parametri porta, filtro e swr
        """
        filtro = self._wrapper.filtro[-4:]
        swr = self._wrapper.swr
        folder = "\\".join([self.comp_folder, self._porta,
                            filtro,
                            swr])
        #trova il punto calibrato più vicino
        filename = str(self._wrapper.pos)
        chdir(folder)
        #trova l'indice delle frequenza più vicina a freq
        freq_line = int(round((freq/1000000 - 450) / 5) + 4)
        with open(filename + ".s2p", 'r') as f:
            return list(islice(f, freq_line-1, freq_line))[0]

    def findCavo(self, freq, who):
        """Con la variabile who va passato il tipo di cavo di cui si vuole
        recuperare i parametri s; in particolare si ha:\n
        a: Cavo collegato tra porta A e CMU\n
        b: Cavo collegato tra porta B e spettro\n
        c: I collegamento tra porta C e modulo\n
        e: Pista CWG
        """
        filename = None
        try:
            assert who in ["c","a","b","e"]
            if who == "c":
                filename = self._wrapper.transC
            elif who == "b":
                filename = self._wrapper.transB
            elif who == "a":
                filename = self._wrapper.transA
            else:
                filename = self._wrapper.transCwg
            freq_line = int(round((freq/1000000 - 450) / 5) + 4)
            return linecache.getline(filename, freq_line)
        except AssertionError:
            print "Inserisci un parametro valido tra (i,e,s,c)"

    def getS(self, freq):
        """Funziona che calcola e restituisce la matrice S del sistema
        composto da cavo verso strumento (CMU, FSV...), I collegamento fra
        modulo e porta C tuner e la pista della CWG su cui è montato il
        modulo. E' importantissimo l'ordine di inserimento"""
        v = []
        if self._wrapper.transCwg:
            v.append(self._matrici.creaMatrice(self.findCavo(freq, "e"), "t"))
        if self._wrapper.transC:
            v.append(self._matrici.creaMatrice(self.findCavo(freq, "c"), "t"))
        v.append(self._matrici.creaMatrice(self.find(freq), "t"))
        if self._porta == "PortA" and self._wrapper.transA:
            v.append(self._matrici.creaMatrice(self.findCavo(freq, "a"), "t"))
        elif self._porta == 'PortB' and self._wrapper.transB:
            v.append(self._matrici.creaMatrice(self.findCavo(freq, "b"), "t"))
        catena = self.embedding(v, len(v) - 1)
        return self._matrici.t_to_s(catena)
            
    #funzione di embedding ricorsiva, indipendente dal numero di transducer
    #selezionati
    def embedding(self, mat_v, index):
        if index == 0:
            return mat_v[index]
        else:
            return self.embedding(mat_v, index - 1) * mat_v[index]
