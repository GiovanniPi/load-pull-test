LoadPull
========
1-Scopo
-----
Il programma permette di interfacciarsi con il sistema di misura load pull e misurare, anche contemporaneamente, 
la potenza in uscita dal EUT, la sensibilita', le armoniche e i consumi

2-Abbreviazioni
-------------
EUT   :Equipment Under Test  
RF    :Radio Frequency

3-Lista dispositivi
-------------------
  * Spectrum analyzer mod.FSV13
  * CMU200 ser. 837983/014(GSM only) or CMU200 ser. (GSM+UMTS)
  * DC PowerSupply HMP2020 
  * DC PowerSupply E3615A
  * Line stretcher
  
4-Procedura di Test
-------------------
  1. Inserire un nome per la sessione di test nel campo *Nome Sessione* e premere invio. Nella log list
  sulla destra dello schermo verra' visualizzato il nome digitato.
  
  2. Inserire una banda tramite *Parametri->Seleziona una banda*. L'utente deve inserire manualamente i 
  parametri della cmu in particolare:
    * GSM850: TCH 187; PL = 5
	* GMS900: TCH 30; PL = 5
	* GSM1800: TCH 700; PL = 0
	* GSM1900: TCH 661; PL = 0
	* UMTS BAND1: TCH 9750; RMC
	* UMTS BAND2: TCH 9400; RMC
	* UMTS BAND4: TCH 1415; RMC
	* UMTS BAND5: TCH 4182; RMC
	* UMTS BAND8: TCH 2787; RMC  
	
	E' consigliato un valore di attenuazione intorno ai 26dB per ingresso e uscita.
	
  3. Nel menu *Misure* selezionare il tipo di misura da fare; si possono selezionare piu' misure
  contemporaneamente. **NB**: Se le misure di potenza e sensibilita' non sono editabili, la banda selezionata 
  non e' supportata dalla CMU collegata al sistema. In tal caso si puo' effetturare una misura di potenza usando 
  l'analizzatore di spettro.
  
  4. Selezionare i transducer da applicare alla misura dal menu *Transducer*. **NB**: I transducer necessari al 
  funzionamento del programma dipendono dal tipo di misure selezionate e sono riportati nella tabella sottostante;
  il transducer CWG e' sempre opzionale.  

  5. Selezionare il tipo di carico da applicare tramite la radio box *Seleziona VSWR*.
  
  6. Azzerare la guida tramite il pulsante *Reset*. Quando la guida sara' in posizione vicino alla 
  barra di progresso verra' visualizzato il messaggio *"Ready"*
  
  7. Per iniziare il test clicca il bottone *"Start"*. **NB** Il programma eseguira' il controllo sulla
  correttezza dei parametri di misura inseriti e avverte in caso di errori o mancanze. Se passa tutti i
  test inizia la misura e apparita' il messaggio *'Testing'* accompagnato da un delizioso motivetto.
  
  8. In caso avvengano errori durante la misura, il programma avvertirà l'utente dell'errore avenuto e
  permettera' di riprendere la misura, oppure uscire. Alla fine di ogni singolo test, comparira' sulla
  carta di smith la posizione dell'ultimo punto misurato e il valore associato sara' visualizzato nel box 
  *Ultimo valore misurato*.
  
  9. A fine misura comparira' un box che avverte l'utente del completamento, accompagnato da un ancora
  più bel motivetto.
  
  10. **Per eseguire un'altra misura resettare la guida.**
  
6-Test disponibili
------------------
###Misura armoniche

Per effettuare una misura di armoniche selezionare *Misure->Misura armonici*. Questa misura
necessita la presenza dell'analizzatore di spettro collegata al sistema FSV.

###Misura consumi

Per effettuare una misura di consumi selezionare *Misure->Misura consumi*. Per questa misura
l'EUT deve essere alimentato tramite la porta dello strumento HMP2020. **NB:** Il settaggio di corrente e tensione 
va fatta dall'utente.

###Misura potenza

Per effettuare una misura di potenza selezionare *Misure->Misura potenza*. Serve una CMU 
in grado di supportare la banda desiderata. 

###Misura sensibilita'

Per effettuare una misura di potenza selezionare *Misure->Misura sensibilita'*. Serve una CMU in grado di 
supportare la banda desiderata.

###Misure potenza FSV

er effettuare una misura di potenza utilizzando l'analizzatore di spettro selezionare *Misure->Misura potenza FSV*
Questa misura e' disponbile solo se la CMU non supporta (o non e' presente) la banda selezionata; serve
un'analizzatore di spettro per eseguirla.

7-Tool aggiuntivi
-----------------

###Calibrazione tuner

Semplicemente: **NON usarla**

###Calibrazione cavo

Cliccando *Strumenti->Calibrazione cavo* compare una finestra che permette di creare il transducer di 
una linea o cavo. Per farla serve l'analizzatore vettoriale di spettro ZNB.