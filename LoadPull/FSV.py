# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 09:47:33 2014

@author: GiovanniPi
"""
import visa
import HandlerIndirizzi as HI

class ErroriFSV(Exception):
    
    def __init__(self, value):
        self._value = value
        
    def __str__(self):
        return repr(self._value)
        

class FSV(object):
    
    validDet = ["ape","neg","pos","samp","rms","aver"]
    
    def __init__(self):
        try:
            rm = visa.ResourceManager()
            self._fsv = rm.open_resource(HI.indirizzi.get('fsv'))
            self.ON
        except visa.VisaIOError:
            raise ErroriFSV("FSV non collegato o spento")
            
    @property
    def idn(self):
        """Ritorna stringa identificazione strumento"""
        return self._fsv.query("*IDN?")
        
    @property
    def rst(self):
        """Resetta strumento"""
        self._fsv.write("*RST")
        
    @property
    def ON(self):
        """Visualizza il display mentre lo strumento è in remoto"""
        self._fsv.write("system:display:update ON")
        
    def setCenterFreq(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            assert valore.isalnum()
            self._fsv.write("freq:cent %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriFSV("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura separati da spazio
                            (max. 13.5 GHz)""")
    
    def readCenterFreq(self):
        return self._fsv.query("freq:cent?")
        
    centerFreq = property(readCenterFreq, setCenterFreq, doc = """Imposta la 
        frequenza centrale, valore inserito è composto da
        valore numerico seguito da unita di misura (va lasciato spazio fra i 
        due)""")
    
    def setSpan(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["ghz","mhz","khz","hz"]
            assert valore.isalnum()
            self._fsv.write("freq:span %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriFSV("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura separati da spazio
                            """)
                            
    def readSpan(self):
        return self._fsv.query("freq:span?")
        
    span = property(readSpan, setSpan, doc =  """Imposta lo span, il
                valore inserito è composto da
        valore numerico seguito da unita di misura (va lasciato spazio fra i 
        due)""")
    
    def setDetector(self, det):
        try:
            assert det in self.validDet
            self._fsv.write("det %s" % det)
        except AssertionError:
            raise ErroriFSV("Detector non valido")
            
    def readDetector(self):
        return self._fsv.query("det?")
        
    detector = property(readDetector, setDetector, doc =  """Seleziona un
                        detector""")
    
    def setMarker(self):
        """Pone il marker sul picco massimo"""
        self._fsv.write("calc:mark:max")
        
    def readMarker(self):
        """Legge il marker e ritorna due valori di tipo stringa con i valori di
        frequenza in hz e potenza in dbm"""
        x = self._fsv.query("calc:mark:x?")
        y = self._fsv.query("calc:mark:y?")
        return x, y
    
    def setTrace(self, mod):
        try:
            assert mod.lower() in ["maxh","writ"]
            self._fsv.write("disp:trac:mode %s" % mod)
        except AssertionError:
            raise ErroriFSV("Formato non valido")
        
    def readTrace(self):
        return self._fsv.query("disp:trac:mode?")
        
    trace = property(readTrace, setTrace, doc = """Setta la modalita della traccia""")

    def setRbw(self, value):
        try:
            valore, unita = value.split()
            assert unita.lower() in ["mhz","khz","hz"]
            assert valore.isalnum()
            self._fsv.write("band %s%s" % (valore, unita))
        except (AssertionError, ValueError):
            raise ErroriFSV("""Inserisci una stringa composta dal valore di 
                            frequenza più unità di misura separati da spazio
                            """)

    def getRbw(self):
        return self._fsv.query("band?")

    rbw = property(getRbw, setRbw, """Setta la RBW""")

    def setType(self):
        self._fsv.write("swe:type swe")
        
    @property
    def sweepType(self):
        return self._fsv.query("swe:type?")
        
    def setAttenuatore(self, value):
        try:        
            assert value >= 0
            self._fsv.write("inp:att %ddB" % value)
        except AssertionError:
            raise ErroriFSV("Attenuazione deve essere un valore positivo")
            
    def getAttenuatore(self):
        return self._fsv.query("inp:att?")
        
    att = property(getAttenuatore, setAttenuatore, """Setta l'attenuatore""")
    
#    def gtl(self):
#        rm = visa.ResourceManager()
#        from pyvisa import resources
#        res = resources.GPIBInterface(rm, self._fsv)
#        res.control_ren('local')