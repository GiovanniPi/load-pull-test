# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\emclab1\Documents\load-pull-test\LoadPull\LoadPull.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_LoadPull(object):
    def setupUi(self, LoadPull):
        LoadPull.setObjectName(_fromUtf8("LoadPull"))
        LoadPull.resize(1156, 849)
        LoadPull.setStyleSheet(_fromUtf8("QMainWindow{\n"
"    background-color: #272927;\n"
"}\n"
"\n"
"QMainWindow::separator{\n"
"    background-color: #272927;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #000;\n"
"    spacing: 3px; \n"
"}\n"
"\n"
"QMainWindow::separator:hover{\n"
"   background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #65f563, stop:1 #a6eba6);\n"
"   padding-left: 4px;\n"
"   border: 1px solid #000;\n"
"   spacing: 3px;\n"
"}\n"
"\n"
"QMessageBox{\n"
"    background-color: #272927;\n"
"}\n"
"\n"
"QInputDialog{\n"
"    background-color: #272927;\n"
"}\n"
"\n"
"QWidget{\n"
"    color: #c1c1c1;\n"
"    background-color: #272927;\n"
"}\n"
"\n"
"QWidget:disabled{\n"
"    color: #484948;\n"
"}\n"
"\n"
"QWidget:item:hover{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #338033, stop: 1 #5c995c);\n"
"    color: #272927;\n"
"}\n"
"\n"
"QWidget:item:selected{\n"
"    background-color: QLinearGradient(x1: 0, y1:0, x2: 1, y2: 1, stop: 0 #65f563, stop: 1 #a6eba6);\n"
"    selection-color: #272927;\n"
"}\n"
"\n"
"QMenuBar:item{\n"
"    background: transparent;\n"
"}\n"
"\n"
"QMenuBar:item:selected{\n"
"    background: transparent;\n"
"    border: 1px solid #65f563;\n"
"}\n"
"\n"
"QMenuBar::item:pressed{\n"
"    border: 1px solid #000;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #1f211f, stop:1 #232523);\n"
"    margin-bottom:-1px;\n"
"    padding-bottom:1px;\n"
"}\n"
"\n"
"QMenu{\n"
"    border: 1px solid #000000;\n"
"}\n"
"\n"
"QMenu::item{\n"
"    padding: 2px 20px 2px 20px;\n"
"}\n"
"\n"
"QMenu::item:selected{\n"
"    color: #272927;\n"
"}\n"
"\n"
"QMenu::indicator:checked{\n"
"    image: url(:/checked.png);\n"
"    height: 9px;\n"
"    width: 9px;\n"
"}\n"
"\n"
"QMenu::indicator:checked:selected{\n"
"    image: url(:/checked_neg.png);\n"
"    height: 9px;\n"
"    width: 9px;\n"
"}\n"
"\n"
"QMenu::right-arrow{\n"
"    image: url(:/right_scroll.png)\n"
"}\n"
"\n"
"QMenu::separator{\n"
"    height: 1px;\n"
"    background-color: #888888;\n"
"    padding-left: 4px;\n"
"    margin-left: 5px;\n"
"    margin-right: 5px;\n"
"}\n"
"\n"
"QComboBox{\n"
"    background-color: QLinearGradient(x1: 0, y1:0, x2: 1, y2: 1, stop: 0 #5a5a5a, stop: 1 #6b6b6b);\n"
"    border-style: solid;\n"
"    border: 1px solid #888888;\n"
"    border-radius: 5;\n"
"}\n"
"\n"
"QComboBox:on{\n"
"    padding-top: 3px;\n"
"    padding-left: 4px;\n"
"    background-color: QLinearGradient(x1: 0, y1:0, x2: 1, y2: 1, stop: 0 #2c2c2c, stop: 1 #3b3b3b);\n"
"}\n"
"\n"
"QComboBox QAbstractItemView{\n"
"    border: 2px solid #888888;\n"
"    selection-background-color: QLinearGradient(x1: 0, y1:0, x2: 1, y2: 1, stop: 0 #65f563, stop: 1 #a6eba6);\n"
"    selection-color: #272927;\n"
"}\n"
"\n"
"QComboBox::drop-down{\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 1px;\n"
"    border-left-color: #888888;\n"
"    border-left-style: solid;\n"
"    border-top-right-radius: 3; \n"
"    border-bottom-right-radius: 3;\n"
"}\n"
"\n"
"QComboBox::down-arrow{\n"
"    image: url(:/down_combo.png);\n"
"}\n"
"\n"
"QPushButton{\n"
"    color: #c1c1c1;\n"
"    background-color: QLinearGradient(x1: 0, y1:0, x2: 1, y2: 1, stop: 0 #5a5a5a, stop: 1 #6b6b6b);\n"
"    border-width: 1px;\n"
"    border-color: #888888;\n"
"    border-style: solid;\n"
"    border-radius: 5;\n"
"    padding: 2px;\n"
"    font-size: 12px;\n"
"    padding-left: 2px;\n"
"    padding-right: 2px;\n"
"}\n"
"\n"
"QPushButton::checked, QPushButton:pressed{\n"
"    color: #c1c1c1;\n"
"    background-color: QLinearGradient(x1: 0, y1:0, x2: 1, y2: 1, stop: 0 #2c2c2c, stop: 1 #3b3b3b);\n"
"    border: 1px solid #65f563;\n"
"}\n"
"\n"
"QComboBox::hover, QPushButton::hover{\n"
"    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #65f563, stop: 1 #a6eba6);\n"
"}\n"
"\n"
"QLineEdit{\n"
"    color: #272927;\n"
"    background-color: QLinearGradient(x1: 0, y1:0, x2: 0, y2: 1, stop: 0 #919291, stop: 1 #818581);\n"
"    border-radius: 5;\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    border-color: #000000;\n"
"    font-size: 12px;\n"
"    selection-background-color: #294629;\n"
"}\n"
"\n"
"QLabel{\n"
"    color: #c1c1c1;\n"
"}\n"
"\n"
"QGroupBox{\n"
"    border: 2px solid #888888;\n"
"    border-radius: 5;\n"
"    padding: 8px;\n"
"    margin-top: 1ex;\n"
"}\n"
"\n"
"QGroupBox::title{\n"
"    color: #c1c1c1;\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center;\n"
"    padding: -5px 5px;\n"
"}\n"
"\n"
"QStatusBar{\n"
"    color: #c1c1c1;\n"
"}\n"
"\n"
"QStatusBar::item{\n"
"    border: 1px;\n"
"}\n"
"\n"
"QRadioButton::indicator:hover, QCheckBox::indicator:hover{\n"
"    border: 1px solid #65f563;\n"
"}\n"
"\n"
"QCheckBox::indicator, QAbstractItemView::indicator{\n"
"    border: 1px solid #888888;\n"
"    width: 10px;\n"
"    height: 10px;\n"
"}\n"
"\n"
"QRadioButton::indicator{\n"
"    border-radius: 6px;\n"
"    border: 1px solid #888888;\n"
"}\n"
"\n"
"QRadioButton::indicator:checked{\n"
"    background-color: qradialgradient(\n"
"        cx: 0.5, cy: 0.5,\n"
"        fx: 0.5, fy: 0.5,\n"
"        radius: 1.0,\n"
"        stop: 0.25 #65f563,\n"
"        stop: 0.3 #272927\n"
"    );\n"
"}\n"
"\n"
"QCheckBox::indicator:checked, QAbstractItemView::indicator:checked{\n"
"    image: url(:/checked.png)\n"
"}\n"
"\n"
"QAbstractItemView::indicator:selected{\n"
"    background-color: #272927;\n"
"}\n"
"\n"
"QRadioButton::indicator:disabled, QCheckBox::indicator:disabled{\n"
"    border: 1px solid #484948;\n"
"}\n"
"\n"
"QAbstractItemView{\n"
"    border: 1px solid #888888;\n"
"    background-color: QLinearGradient(x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #323232, stop: 0.7 #383838, stop: 0.9 #4d4d4d, stop: 1 #323232);\n"
"}\n"
"\n"
"QAbstractItemView::item:hover{\n"
"    selection-background-color: red;\n"
"}\n"
"\n"
"QAbstractItemView::indicator{\n"
"    border: 1px solid #888888;\n"
"    height: 10px;\n"
"    width: 10px;\n"
"}\n"
"\n"
"QAbstractItemView::indicator:hover{\n"
"    border: 1px solid #272927;\n"
"}\n"
"\n"
"QScrollBar:vertical{\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"    width: 11px;\n"
"    margin: 16px 0 16px 0;\n"
"    border: 1px solid #121212;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical{\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #65f563, stop: 0.5 #5c995c, stop: 1 #65f563);\n"
"    min-height: 20px;\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical{\n"
"    border: 1px solid #65f563;\n"
"    border-radius: 2px;\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #323232, stop: 1 #383838);\n"
"    height: 14px;\n"
"    subcontrol-position: bottom;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical{\n"
"    border: 1px solid #65f563;\n"
"    border-radius: 2px;\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #323232, stop: 1 #383838);\n"
"    height: 14px;\n"
"    subcontrol-position: top;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical{\n"
"    image: url(:/up_scroll.png);\n"
"    height: 9px;\n"
"    width: 7px;\n"
"}\n"
"\n"
"QScrollBar::down-arrow:vertical{\n"
"    image: url(:/down_scroll.png);\n"
"    height: 9px;\n"
"    width: 7px;\n"
"}\n"
"\n"
"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical, QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal{\n"
"    background: none;\n"
"}\n"
"\n"
"QScrollBar:horizontal{\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"    height: 11px;\n"
"    margin: 0px 16px 0px 16px;\n"
"    border: 1px solid #121212;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal{\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #65f563, stop: 0.5 #5c995c, stop: 1 #65f563);\n"
"    min-height: 20px;\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal{\n"
"    border: 1px solid #65f563;\n"
"    border-radius: 2px;\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #323232, stop: 1 #383838);\n"
"    width: 14px;\n"
"    subcontrol-position: right;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal{\n"
"    border: 1px solid #65f563;\n"
"    border-radius: 2px;\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #323232, stop: 1 #383838);\n"
"    width: 14px;\n"
"    subcontrol-position: left;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::left-arrow:horizontal{\n"
"    image: url(:/left_scroll.png);\n"
"    height: 9px;\n"
"    width: 7px;\n"
"}\n"
"\n"
"QScrollBar::right-arrow:horizontal{\n"
"    image: url(:/right_scroll.png);\n"
"    height: 9px;\n"
"    width: 7px;\n"
"}\n"
"\n"
"QScrollBar::add-line:hover, QScrollBar::sub-line:hover, QScrollBar::handle:hover{\n"
"    border: 2px solid #65f563;\n"
"}\n"
"\n"
"QDockWidget{\n"
"    titlebar-close-icon: url(:/close_dock.png);\n"
"    titlebar-normal-icon: url(:/float_dock.png);\n"
"}\n"
"\n"
"QDockWidget::title{\n"
"    background-color: #272927;\n"
"    border: 1px solid #888888;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QDockWidget::close-button, QDockWidget::float-button{\n"
"    icon-size: 8px;\n"
"}\n"
"\n"
"QDockWidget::close-button:hover, QDockWidget::float-button:hover{\n"
"    border: 1px solid #65f563;\n"
"    border-radius: 4px;\n"
"    width: 8px;\n"
"    height: 8px;\n"
"}\n"
"\n"
"QSpinBox{\n"
"    padding-right: 15px; \n"
"    background-color: QLinearGradient(x1: 0, y1:0, x2: 1, y2: 1, stop: 0 #5a5a5a, stop: 1 #6b6b6b);\n"
"    border-style: solid;\n"
"    border: 1px solid #888888;\n"
"    border-radius: 5;\n"
"}\n"
"\n"
"QSpinBox::up-button{\n"
"    subcontrol-origin: border;\n"
"    subcontrol-position: top right;\n"
"    width: 16px;\n"
"    border-image: url(:/up_border_scroll.png);\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #323232, stop: 1 #383838);\n"
"}\n"
"\n"
"QSpinBox::up-arrow {\n"
"    image: url(:/up_spin.png);\n"
"    width: 7px;\n"
"    height: 7px;\n"
"}\n"
"\n"
"QSpinBox::up-arrow:disabled, QSpinBox::up-arrow:off {\n"
"    image: url(:/images/up_arrow_disabled.png);\n"
"}\n"
"\n"
"QSpinBox::down-button {\n"
"    subcontrol-origin: border;\n"
"    subcontrol-position: bottom right;\n"
"    width: 16px;\n"
"    border-image: url(:/down_border_scroll.png);\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #323232, stop: 1 #383838);\n"
"}\n"
"\n"
"QSpinBox::down-arrow {\n"
"    image: url(:/down_spin.png);\n"
"    width: 7px;\n"
"    height: 7px;\n"
"}\n"
"\n"
"QSpinBox::down-arrow:disabled, QSpinBox::down-arrow:off{\n"
"   image: url(:/images/down_arrow_disabled.png);\n"
"}\n"
"\n"
"QToolTip{\n"
"    color: #c1c1c1;\n"
"    border-radius: 5px;\n"
"    border: 1px solid #65f563;\n"
"    background-color: #272927;\n"
"}\n"
"\n"
"QTabBar::tab{\n"
"    color: #c1c1c1;\n"
"    border: 1px solid #000;\n"
"    border-bottom-style: none;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:1 #212121, stop:.4 #343434);\n"
"    padding-left: 10px;\n"
"    padding-right: 10px;\n"
"    padding-top: 3px;\n"
"    padding-bottom: 2px;\n"
"    margin-right: -1px;\n"
"}\n"
"\n"
"QTabWidget::pane {\n"
"    border: 1px solid #000000;\n"
"    top: 1px;\n"
"}\n"
"\n"
"QTabBar::tab:last{\n"
"    margin-right: 0; \n"
"    border-top-right-radius: 3px;\n"
"}\n"
"\n"
"QTabBar::tab:first:!selected{\n"
"    margin-left: 0px; \n"
"    border-top-left-radius: 3px;\n"
"}\n"
"\n"
"QTabBar::tab:!selected{\n"
"    color: #b1b1b1;\n"
"    border-bottom-style: solid;\n"
"    margin-top: 3px;\n"
"    background-color: transparent;\n"
"}\n"
"\n"
"QTabBar::tab:selected{\n"
"    border-top-left-radius: 3px;\n"
"    border-top-right-radius: 3px;\n"
"    margin-bottom: 0px;\n"
"}\n"
"\n"
"QTabBar::tab:!selected:hover{\n"
"    border-top: 2px solid #65f563;\n"
"    padding-bottom: 3px;\n"
"    border-top-left-radius: 3px;\n"
"    border-top-right-radius: 3px;\n"
"}\n"
"\n"
"QSlider::groove:horizontal{\n"
"    border: 1px solid #000;\n"
"    height: 2px; \n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0.0 #484848, stop: 0.5 #282828, stop: 1 #484848);\n"
"    margin: 2px 0;\n"
"    color: #c1c1c1;\n"
"    text-align: bottom;\n"
"}\n"
"\n"
"QSlider::handle:horizontal{\n"
"    background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #65f563, stop: 0.5 #5c995c, stop: 1 #65f563);\n"
"    width: 18px;\n"
"    height: 5px;\n"
"    margin: -3px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}\n"
"\n"
"QProgressBar{\n"
"    border: 1px solid #000;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"    height: 6px;\n"
"}\n"
"\n"
"QProgressBar::chunk{\n"
"    background-color: #5c995c;\n"
"    width: 3.5px;\n"
"    margin: 0.5px;\n"
"}"))
        self.centralwidget = QtGui.QWidget(LoadPull)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 191, 81))
        self.groupBox.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.label = QtGui.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(10, 20, 171, 20))
        self.label.setObjectName(_fromUtf8("label"))
        self.edit_name = QtGui.QLineEdit(self.groupBox)
        self.edit_name.setGeometry(QtCore.QRect(10, 50, 171, 20))
        self.edit_name.setObjectName(_fromUtf8("edit_name"))
        self.groupBox_2 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 100, 191, 301))
        self.groupBox_2.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.swr_1 = QtGui.QRadioButton(self.groupBox_2)
        self.swr_1.setGeometry(QtCore.QRect(20, 30, 82, 17))
        self.swr_1.setObjectName(_fromUtf8("swr_1"))
        self.group_swr = QtGui.QButtonGroup(LoadPull)
        self.group_swr.setObjectName(_fromUtf8("group_swr"))
        self.group_swr.addButton(self.swr_1)
        self.swr_1_5 = QtGui.QRadioButton(self.groupBox_2)
        self.swr_1_5.setGeometry(QtCore.QRect(20, 60, 82, 17))
        self.swr_1_5.setObjectName(_fromUtf8("swr_1_5"))
        self.group_swr.addButton(self.swr_1_5)
        self.swr_2 = QtGui.QRadioButton(self.groupBox_2)
        self.swr_2.setGeometry(QtCore.QRect(20, 90, 82, 17))
        self.swr_2.setObjectName(_fromUtf8("swr_2"))
        self.group_swr.addButton(self.swr_2)
        self.swr_3 = QtGui.QRadioButton(self.groupBox_2)
        self.swr_3.setGeometry(QtCore.QRect(20, 120, 82, 17))
        self.swr_3.setObjectName(_fromUtf8("swr_3"))
        self.group_swr.addButton(self.swr_3)
        self.swr_4 = QtGui.QRadioButton(self.groupBox_2)
        self.swr_4.setGeometry(QtCore.QRect(20, 150, 82, 17))
        self.swr_4.setObjectName(_fromUtf8("swr_4"))
        self.group_swr.addButton(self.swr_4)
        self.swr_6 = QtGui.QRadioButton(self.groupBox_2)
        self.swr_6.setGeometry(QtCore.QRect(20, 180, 82, 17))
        self.swr_6.setObjectName(_fromUtf8("swr_6"))
        self.group_swr.addButton(self.swr_6)
        self.swr_10 = QtGui.QRadioButton(self.groupBox_2)
        self.swr_10.setGeometry(QtCore.QRect(20, 210, 82, 17))
        self.swr_10.setObjectName(_fromUtf8("swr_10"))
        self.group_swr.addButton(self.swr_10)
        self.swr_open = QtGui.QRadioButton(self.groupBox_2)
        self.swr_open.setGeometry(QtCore.QRect(20, 240, 82, 20))
        self.swr_open.setObjectName(_fromUtf8("swr_open"))
        self.group_swr.addButton(self.swr_open)
        self.swr_full_test = QtGui.QRadioButton(self.groupBox_2)
        self.swr_full_test.setGeometry(QtCore.QRect(20, 270, 82, 17))
        self.swr_full_test.setObjectName(_fromUtf8("swr_full_test"))
        self.group_swr.addButton(self.swr_full_test)
        self.groupBox_3 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 410, 191, 311))
        self.groupBox_3.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.label_2 = QtGui.QLabel(self.groupBox_3)
        self.label_2.setGeometry(QtCore.QRect(10, 20, 51, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.groupBox_3)
        self.label_3.setGeometry(QtCore.QRect(10, 40, 61, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.groupBox_3)
        self.label_4.setGeometry(QtCore.QRect(10, 60, 61, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(self.groupBox_3)
        self.label_5.setGeometry(QtCore.QRect(10, 80, 61, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_6 = QtGui.QLabel(self.groupBox_3)
        self.label_6.setGeometry(QtCore.QRect(10, 100, 61, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(self.groupBox_3)
        self.label_7.setGeometry(QtCore.QRect(10, 140, 61, 16))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_8 = QtGui.QLabel(self.groupBox_3)
        self.label_8.setGeometry(QtCore.QRect(10, 180, 61, 16))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.label_9 = QtGui.QLabel(self.groupBox_3)
        self.label_9.setGeometry(QtCore.QRect(10, 220, 61, 16))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.lbl_a_trans = QtGui.QLabel(self.groupBox_3)
        self.lbl_a_trans.setGeometry(QtCore.QRect(20, 160, 141, 20))
        self.lbl_a_trans.setObjectName(_fromUtf8("lbl_a_trans"))
        self.lbl_b_trans = QtGui.QLabel(self.groupBox_3)
        self.lbl_b_trans.setGeometry(QtCore.QRect(20, 200, 141, 20))
        self.lbl_b_trans.setObjectName(_fromUtf8("lbl_b_trans"))
        self.lbl_c_trans = QtGui.QLabel(self.groupBox_3)
        self.lbl_c_trans.setGeometry(QtCore.QRect(20, 240, 141, 20))
        self.lbl_c_trans.setObjectName(_fromUtf8("lbl_c_trans"))
        self.label_10 = QtGui.QLabel(self.groupBox_3)
        self.label_10.setGeometry(QtCore.QRect(10, 262, 61, 16))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.lbl_cwg_trans = QtGui.QLabel(self.groupBox_3)
        self.lbl_cwg_trans.setGeometry(QtCore.QRect(20, 280, 141, 20))
        self.lbl_cwg_trans.setObjectName(_fromUtf8("lbl_cwg_trans"))
        self.lbl_band = QtGui.QLabel(self.groupBox_3)
        self.lbl_band.setGeometry(QtCore.QRect(100, 21, 61, 16))
        self.lbl_band.setObjectName(_fromUtf8("lbl_band"))
        self.lbl_freq = QtGui.QLabel(self.groupBox_3)
        self.lbl_freq.setGeometry(QtCore.QRect(100, 40, 61, 16))
        self.lbl_freq.setObjectName(_fromUtf8("lbl_freq"))
        self.lbl_lambda = QtGui.QLabel(self.groupBox_3)
        self.lbl_lambda.setGeometry(QtCore.QRect(100, 60, 61, 16))
        self.lbl_lambda.setObjectName(_fromUtf8("lbl_lambda"))
        self.lbl_min = QtGui.QLabel(self.groupBox_3)
        self.lbl_min.setGeometry(QtCore.QRect(100, 100, 61, 16))
        self.lbl_min.setObjectName(_fromUtf8("lbl_min"))
        self.lbl_max = QtGui.QLabel(self.groupBox_3)
        self.lbl_max.setGeometry(QtCore.QRect(100, 80, 61, 16))
        self.lbl_max.setObjectName(_fromUtf8("lbl_max"))
        self.graph = SmithChart(self.centralwidget)
        self.graph.setGeometry(QtCore.QRect(210, 20, 700, 731))
        self.graph.setObjectName(_fromUtf8("graph"))
        self.groupBox_4 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_4.setGeometry(QtCore.QRect(920, 10, 221, 781))
        self.groupBox_4.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox_4.setObjectName(_fromUtf8("groupBox_4"))
        self.list_log = QtGui.QListWidget(self.groupBox_4)
        self.list_log.setGeometry(QtCore.QRect(10, 20, 201, 751))
        self.list_log.setObjectName(_fromUtf8("list_log"))
        self.groupBox_5 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_5.setGeometry(QtCore.QRect(10, 730, 191, 61))
        self.groupBox_5.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox_5.setObjectName(_fromUtf8("groupBox_5"))
        self.btn_start = QtGui.QPushButton(self.groupBox_5)
        self.btn_start.setGeometry(QtCore.QRect(10, 20, 75, 23))
        self.btn_start.setObjectName(_fromUtf8("btn_start"))
        self.btn_reset = QtGui.QPushButton(self.groupBox_5)
        self.btn_reset.setGeometry(QtCore.QRect(100, 20, 75, 23))
        self.btn_reset.setObjectName(_fromUtf8("btn_reset"))
        self.lbl_last_point = QtGui.QLabel(self.centralwidget)
        self.lbl_last_point.setGeometry(QtCore.QRect(210, 750, 701, 16))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.lbl_last_point.setFont(font)
        self.lbl_last_point.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_last_point.setObjectName(_fromUtf8("lbl_last_point"))
        LoadPull.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(LoadPull)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1156, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuTransducers = QtGui.QMenu(self.menubar)
        self.menuTransducers.setObjectName(_fromUtf8("menuTransducers"))
        self.menuTests = QtGui.QMenu(self.menubar)
        self.menuTests.setObjectName(_fromUtf8("menuTests"))
        self.menuParameters = QtGui.QMenu(self.menubar)
        self.menuParameters.setObjectName(_fromUtf8("menuParameters"))
        self.menuPoint_Reduction = QtGui.QMenu(self.menuParameters)
        self.menuPoint_Reduction.setObjectName(_fromUtf8("menuPoint_Reduction"))
        self.menuTools = QtGui.QMenu(self.menubar)
        self.menuTools.setObjectName(_fromUtf8("menuTools"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        LoadPull.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(LoadPull)
        self.statusbar.setSizeGripEnabled(False)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        LoadPull.setStatusBar(self.statusbar)
        self.menu_transducer_a = QtGui.QAction(LoadPull)
        self.menu_transducer_a.setObjectName(_fromUtf8("menu_transducer_a"))
        self.menu_transducer_b = QtGui.QAction(LoadPull)
        self.menu_transducer_b.setObjectName(_fromUtf8("menu_transducer_b"))
        self.menu_transducer_c = QtGui.QAction(LoadPull)
        self.menu_transducer_c.setObjectName(_fromUtf8("menu_transducer_c"))
        self.menu_transducer_cwg = QtGui.QAction(LoadPull)
        self.menu_transducer_cwg.setObjectName(_fromUtf8("menu_transducer_cwg"))
        self.act_harmonics = QtGui.QAction(LoadPull)
        self.act_harmonics.setCheckable(True)
        self.act_harmonics.setObjectName(_fromUtf8("act_harmonics"))
        self.act_consumption = QtGui.QAction(LoadPull)
        self.act_consumption.setCheckable(True)
        self.act_consumption.setObjectName(_fromUtf8("act_consumption"))
        self.act_radiation = QtGui.QAction(LoadPull)
        self.act_radiation.setCheckable(True)
        self.act_radiation.setObjectName(_fromUtf8("act_radiation"))
        self.act_power = QtGui.QAction(LoadPull)
        self.act_power.setCheckable(True)
        self.act_power.setObjectName(_fromUtf8("act_power"))
        self.act_power_FSV = QtGui.QAction(LoadPull)
        self.act_power_FSV.setCheckable(True)
        self.act_power_FSV.setObjectName(_fromUtf8("act_power_FSV"))
        self.act_sensibility = QtGui.QAction(LoadPull)
        self.act_sensibility.setCheckable(True)
        self.act_sensibility.setObjectName(_fromUtf8("act_sensibility"))
        self.act_band = QtGui.QAction(LoadPull)
        self.act_band.setObjectName(_fromUtf8("act_band"))
        self.actionNo_reduction = QtGui.QAction(LoadPull)
        self.actionNo_reduction.setObjectName(_fromUtf8("actionNo_reduction"))
        self.actionFull_reduction = QtGui.QAction(LoadPull)
        self.actionFull_reduction.setObjectName(_fromUtf8("actionFull_reduction"))
        self.act_cable_cal = QtGui.QAction(LoadPull)
        self.act_cable_cal.setObjectName(_fromUtf8("act_cable_cal"))
        self.act_tuner_cal = QtGui.QAction(LoadPull)
        self.act_tuner_cal.setObjectName(_fromUtf8("act_tuner_cal"))
        self.act_info = QtGui.QAction(LoadPull)
        self.act_info.setObjectName(_fromUtf8("act_info"))
        self.act_help = QtGui.QAction(LoadPull)
        self.act_help.setObjectName(_fromUtf8("act_help"))
        self.menuTransducers.addAction(self.menu_transducer_a)
        self.menuTransducers.addAction(self.menu_transducer_b)
        self.menuTransducers.addAction(self.menu_transducer_c)
        self.menuTransducers.addAction(self.menu_transducer_cwg)
        self.menuTests.addAction(self.act_harmonics)
        self.menuTests.addAction(self.act_consumption)
        self.menuTests.addAction(self.act_radiation)
        self.menuTests.addAction(self.act_power)
        self.menuTests.addAction(self.act_sensibility)
        self.menuTests.addAction(self.act_power_FSV)
        self.menuPoint_Reduction.addAction(self.actionNo_reduction)
        self.menuPoint_Reduction.addAction(self.actionFull_reduction)
        self.menuParameters.addAction(self.act_band)
        self.menuParameters.addAction(self.menuPoint_Reduction.menuAction())
        self.menuTools.addAction(self.act_cable_cal)
        self.menuTools.addAction(self.act_tuner_cal)
        self.menuHelp.addAction(self.act_info)
        self.menuHelp.addAction(self.act_help)
        self.menubar.addAction(self.menuTransducers.menuAction())
        self.menubar.addAction(self.menuTests.menuAction())
        self.menubar.addAction(self.menuParameters.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(LoadPull)
        QtCore.QObject.connect(self.menu_transducer_a, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onTransducerSelected)
        QtCore.QObject.connect(self.menu_transducer_b, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onTransducerSelected)
        QtCore.QObject.connect(self.menu_transducer_c, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onTransducerSelected)
        QtCore.QObject.connect(self.menu_transducer_cwg, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onTransducerSelected)
        QtCore.QObject.connect(self.act_consumption, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), LoadPull.onTestSelected)
        QtCore.QObject.connect(self.act_harmonics, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), LoadPull.onTestSelected)
        QtCore.QObject.connect(self.act_power, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), LoadPull.onTestSelected)
        QtCore.QObject.connect(self.act_power_FSV, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), LoadPull.onTestSelected)
        QtCore.QObject.connect(self.act_radiation, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), LoadPull.onTestSelected)
        QtCore.QObject.connect(self.act_sensibility, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), LoadPull.onTestSelected)
        QtCore.QObject.connect(self.act_band, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onBandSelected)
        QtCore.QObject.connect(self.act_help, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onHelp)
        QtCore.QObject.connect(self.act_info, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onAbout)
        QtCore.QObject.connect(self.act_cable_cal, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onCableCal)
        QtCore.QObject.connect(self.act_tuner_cal, QtCore.SIGNAL(_fromUtf8("triggered()")), LoadPull.onTunerCal)
        QtCore.QObject.connect(self.group_swr, QtCore.SIGNAL(_fromUtf8("buttonClicked(QAbstractButton*)")), LoadPull.onSwrSelected)
        QtCore.QObject.connect(self.btn_start, QtCore.SIGNAL(_fromUtf8("clicked()")), LoadPull.onStart)
        QtCore.QObject.connect(self.btn_reset, QtCore.SIGNAL(_fromUtf8("clicked()")), LoadPull.onReset)
        QtCore.QMetaObject.connectSlotsByName(LoadPull)

    def retranslateUi(self, LoadPull):
        LoadPull.setWindowTitle(_translate("LoadPull", "Imput impedance tuner", None))
        self.groupBox.setTitle(_translate("LoadPull", "Session", None))
        self.label.setText(_translate("LoadPull", "Insert the name sesssion", None))
        self.groupBox_2.setTitle(_translate("LoadPull", "SWR", None))
        self.swr_1.setText(_translate("LoadPull", "1", None))
        self.swr_1_5.setText(_translate("LoadPull", "1.5", None))
        self.swr_2.setText(_translate("LoadPull", "2", None))
        self.swr_3.setText(_translate("LoadPull", "3", None))
        self.swr_4.setText(_translate("LoadPull", "4", None))
        self.swr_6.setText(_translate("LoadPull", "6", None))
        self.swr_10.setText(_translate("LoadPull", "10", None))
        self.swr_open.setText(_translate("LoadPull", "Open", None))
        self.swr_full_test.setText(_translate("LoadPull", "Full Test", None))
        self.groupBox_3.setTitle(_translate("LoadPull", "Parameters", None))
        self.label_2.setText(_translate("LoadPull", "Band:", None))
        self.label_3.setText(_translate("LoadPull", "Frequency:", None))
        self.label_4.setText(_translate("LoadPull", "Lambda:", None))
        self.label_5.setText(_translate("LoadPull", "Max angle:", None))
        self.label_6.setText(_translate("LoadPull", "Min angle:", None))
        self.label_7.setText(_translate("LoadPull", "Port A tr:", None))
        self.label_8.setText(_translate("LoadPull", "Port B tr:", None))
        self.label_9.setText(_translate("LoadPull", "Port C tr:", None))
        self.lbl_a_trans.setText(_translate("LoadPull", "ND", None))
        self.lbl_b_trans.setText(_translate("LoadPull", "ND", None))
        self.lbl_c_trans.setText(_translate("LoadPull", "ND", None))
        self.label_10.setText(_translate("LoadPull", "CWG tr", None))
        self.lbl_cwg_trans.setText(_translate("LoadPull", "ND", None))
        self.lbl_band.setText(_translate("LoadPull", "ND", None))
        self.lbl_freq.setText(_translate("LoadPull", "ND", None))
        self.lbl_lambda.setText(_translate("LoadPull", "ND", None))
        self.lbl_min.setText(_translate("LoadPull", "ND", None))
        self.lbl_max.setText(_translate("LoadPull", "ND", None))
        self.groupBox_4.setTitle(_translate("LoadPull", "Log", None))
        self.groupBox_5.setTitle(_translate("LoadPull", "Commands", None))
        self.btn_start.setText(_translate("LoadPull", "Start", None))
        self.btn_reset.setText(_translate("LoadPull", "Reset line", None))
        self.lbl_last_point.setText(_translate("LoadPull", "Last point tested", None))
        self.menuTransducers.setTitle(_translate("LoadPull", "Transducers", None))
        self.menuTests.setTitle(_translate("LoadPull", "Tests", None))
        self.menuParameters.setTitle(_translate("LoadPull", "Parameters", None))
        self.menuPoint_Reduction.setTitle(_translate("LoadPull", "Point Reduction", None))
        self.menuTools.setTitle(_translate("LoadPull", "Tools", None))
        self.menuHelp.setTitle(_translate("LoadPull", "Help", None))
        self.menu_transducer_a.setText(_translate("LoadPull", "Port A", None))
        self.menu_transducer_a.setStatusTip(_translate("LoadPull", "Set the additional trransducer for port A", None))
        self.menu_transducer_b.setText(_translate("LoadPull", "Port B", None))
        self.menu_transducer_b.setStatusTip(_translate("LoadPull", "Set the additional trransducer for port B", None))
        self.menu_transducer_c.setText(_translate("LoadPull", "Port C", None))
        self.menu_transducer_c.setStatusTip(_translate("LoadPull", "Set the additional trransducer for port C", None))
        self.menu_transducer_cwg.setText(_translate("LoadPull", "CWG", None))
        self.menu_transducer_cwg.setStatusTip(_translate("LoadPull", "Set the transducer for the CWG", None))
        self.act_harmonics.setText(_translate("LoadPull", "Harmonics", None))
        self.act_harmonics.setStatusTip(_translate("LoadPull", "Select the harmonic test", None))
        self.act_consumption.setText(_translate("LoadPull", "Consumption", None))
        self.act_consumption.setStatusTip(_translate("LoadPull", "Select the power consumption test", None))
        self.act_radiation.setText(_translate("LoadPull", "Radiations", None))
        self.act_radiation.setStatusTip(_translate("LoadPull", "Select the radiations test", None))
        self.act_power.setText(_translate("LoadPull", "Power", None))
        self.act_power.setStatusTip(_translate("LoadPull", "Select the signal power test", None))
        self.act_power_FSV.setText(_translate("LoadPull", "Power FSV", None))
        self.act_power_FSV.setStatusTip(_translate("LoadPull", "select the power test using the FSV", None))
        self.act_sensibility.setText(_translate("LoadPull", "Sensibility", None))
        self.act_sensibility.setStatusTip(_translate("LoadPull", "Select the signal sensibility test", None))
        self.act_band.setText(_translate("LoadPull", "Band", None))
        self.act_band.setStatusTip(_translate("LoadPull", "Select the band to be tested", None))
        self.actionNo_reduction.setText(_translate("LoadPull", "No reduction", None))
        self.actionFull_reduction.setText(_translate("LoadPull", "Full reduction", None))
        self.act_cable_cal.setText(_translate("LoadPull", "Cable cal", None))
        self.act_tuner_cal.setText(_translate("LoadPull", "Tuner cal", None))
        self.act_info.setText(_translate("LoadPull", "Info", None))
        self.act_help.setText(_translate("LoadPull", "Help", None))

from CustomPlot import SmithChart
import icon_rc
