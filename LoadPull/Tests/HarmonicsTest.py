__author__ = 'GiovanniPi'

from Test import Test, TestError
from ..Drivers.FSV import FSV, ErrorFSV
from time import sleep
import numpy as np


class Harmonics(Test):

    def __init__(self, utils):
        try:
            super(Harmonics, self).__init__()
            self.fsv = FSV()
            self.utils = utils
            self.work_dir = None
            self.limit = None
            self.s11 = None
            self.embed = None
        except ErrorFSV as e:
            raise TestError(str(e))

    def beforeStart(self):
        """general settings for the test"""
        self.fsv.att = 0
        self.fsv.rbw = "1 MHz"
        self.fsv.setType()
        if 'GSM' in self.utils.band:
            self.fsv.detector = 'pos'
            self.fsv.span = "5 MHz"
            self.limit = 4000000000
        else:
            self.fsv.detector = 'rms'
            self.fsv.span = "10 MHz"
            self.limit = 12750000000

    def checkFolder(self, *args):
        """Build the folder path"""
        self.work_dir = super(Harmonics, self).checkFolder(self.utils.saveDir, self.utils.band, 'Harmonics', self.utils.swr)

    def start(self):
        """Start the test"""
        self.s11 = self.embed.getS(self.utils.frequency)[0, 0]
        self.readHarmonics(self.utils.frequency * 2, 0, self.work_dir, [])

    def readHarmonics(self, freq, directory, vect):
        """:param freq: centrer frequency to be set on the spectrum analyzer
        :param directory: saving directory
        :param vect: vector that contains the harmonics data, saved as string
        Recoursive routine for reading the harmonics. Exit condition new frequency is higher than the limits fixed
        (4GHz for GSM, 12.75GHz for UMTS/LTE)"""
        self.fsv.trace = "writ"
        self.fsv.centerFreq = str(freq) + " Hz"
        self.fsv.trace = "maxh"
        s = self.embed.getS(freq)
        #stabilize the track
        sleep(4)
        self._fsv.setMarker()
        sleep(0.2)
        x, y = self._fsv.readMarker()
        val = -20*np.log10(np.absolute(complex(s[0, 1]))) + float(y)
        name = directory + "\\" + str(freq) + '.csv'
        with open(name, 'a+') as f:
            line = ",".join([str(self._s11), str(val), '\n'])
            f.write(line)
            vect.append('%s[dBm];' % str(val))
        new_freq = freq + self.utils.frequency
        if new_freq > self.limit:
            self.utils.is_ok = True
            strings = ' '.join(vect)
            self.done.emit([True, 'Harmonics', self.s11, strings])
            return
        else:
            self.readHarmonics(new_freq, directory, vect)
