__author__ = 'GiovanniPi'


from PyQt4 import QtCore


class Test(QtCore.QObject):

    done = QtCore.pyqtSignal([list])

    def beforeStart(self):
        """Operation before the test"""
        pass

    def start(self):
        """Test.start abstract method. Implement the start of the test"""
        pass

    def checkFolder(self, root, *args):
        """"Test.checkFolder build the path of the new folder. Return the path"""
        qdir = QtCore.QDir(root)
        for item in args:
            if qdir.exists(item):
                qdir.mkdir(item)
                qdir.cd(item)
        return qdir.path()


class TestError(Exception):

    def __init__(self, value):
        self.__value = value

    def __str__(self):
        return repr(self.__value)


# class Compensazione(object):
#     """Classe per l'embedding dei transducer, va dichiarata passando il file di
#     wrapper e la porta relatico allo strumento che stiamo analzzando.\n
#     La funzione principale è getS(freq)->matrice S embedding\n
#     -findCavo serve a leggere i parametri del cavo, vengono recuperati in base
#     al parametro porta. I file letti con il metodo linecache, poichè vengono
#     fatti molti accessi sullo stesso file
#     -find() serve a leggere i file di calibraizone, utilizza metodo islice,
#     i cui tempi di accesso sono indipendenti dal numero di riga che va letta"""
#
#     comp_folder = "C:\Users\emclab1\Documents\load-pull-test\LoadPull\Calibrazioni"
#     skip_line = "#","!"
#
#     def __init__(self, wrapper, porta):
#         """Esegue accesso ai file di calibrazione; esegue embedding per trovare
#         la posizione sulla carta di smith di un determinato carico"""
#         self._wrapper = wrapper
#         self._porta = porta
#         self._matrici = Matrici()
#
#     def find(self, freq):
#         """Legge i parametri dai file di calibrazione del tuner, usando i
#         parametri porta, filtro e swr
#         """
#         filtro = self._wrapper.filtro[-4:]
#         swr = self._wrapper.swr
#         folder = "\\".join([self.comp_folder, self._porta,
#                             filtro,
#                             swr])
#         #trova il punto calibrato più vicino
#         filename = str(self._wrapper.pos)
#         chdir(folder)
#         #trova l'indice delle frequenza più vicina a freq
#         freq_line = int(round((freq/1000000 - 450) / 5) + 4)
#         with open(filename + ".s2p", 'r') as f:
#             return list(islice(f, freq_line-1, freq_line))[0]
#
#     def findCavo(self, freq, who):
#         """Con la variabile who va passato il tipo di cavo di cui si vuole
#         recuperare i parametri s; in particolare si ha:\n
#         a: Cavo collegato tra porta A e CMU\n
#         b: Cavo collegato tra porta B e spettro\n
#         c: I collegamento tra porta C e modulo\n
#         e: Pista CWG
#         """
#         filename = None
#         try:
#             assert who in ["c","a","b","e"]
#             if who == "c":
#                 filename = self._wrapper.portaC
#             elif who == "b":
#                 filename = self._wrapper.portaB
#             elif who == "a":
#                 filename = self._wrapper.portaA
#             else:
#                 filename = self._wrapper.cwg
#             freq_line = int(round((freq/1000000 - 450) / 5) + 4)
#             return linecache.getline(filename, freq_line)
#         except AssertionError:
#             print "Inserisci un parametro valido tra (i,e,s,c)"
#
#     def getS(self, freq):
#         """Funziona che calcola e restituisce la matrice S del sistema
#         composto da cavo verso strumento (CMU, FSV...), I collegamento fra
#         modulo e porta C tuner e la pista della CWG su cui è montato il
#         modulo. E' importantissimo l'ordine di inserimento"""
#         v = []
#         if self._wrapper.cwg:
#             v.append(self._matrici.creaMatrice(self.findCavo(freq, "e"), "t"))
#         if self._wrapper.portaC:
#             v.append(self._matrici.creaMatrice(self.findCavo(freq, "c"), "t"))
#         v.append(self._matrici.creaMatrice(self.find(freq), "t"))
#         if self._porta == "PortA" and self._wrapper.portaA:
#             v.append(self._matrici.creaMatrice(self.findCavo(freq, "a"), "t"))
#         elif self._porta == 'PortB' and self._wrapper.portaB:
#             v.append(self._matrici.creaMatrice(self.findCavo(freq, "b"), "t"))
#         catena = self.embedding(v, len(v) - 1)
#         return self._matrici.t_to_s(catena)
#
#     #funzione di embedding ricorsiva, indipendente dal numero di transducer
#     #selezionati
#     def embedding(self, mat_v, index):
#         if index == 0:
#             return mat_v[index]
#         else:
#             return self.embedding(mat_v, index - 1) * mat_v[index]