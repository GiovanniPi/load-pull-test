# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 10:34:03 2014

@author: GiovanniPi
"""

from TouchstoneS2P import TouchstoneS2P
from os import listdir, chdir, makedirs
from datetime import datetime

path = raw_input("Insrisci percorso cartella: ")

lista = listdir(path)
chdir(path)
makedirs("new")

new = TouchstoneS2P()

for element in lista:
    strip = element.split("_")
    new_name = "\\".join(["new", strip[0]])
    
    new.name = new_name
    new.comment = " ".join([str(datetime.now()), strip[0], strip[1], strip[2]])
    new.invertiColonne(element)