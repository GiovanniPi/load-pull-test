# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 12:59:01 2014

@author: GiovanniPi
"""

from TouchstoneS2P import TouchstoneS2P
from os import listdir, chdir, makedirs
from datetime import datetime

path = raw_input("Insrisci percorso cartella: ")

lista = listdir(path)
chdir(path)
makedirs("new")

new = TouchstoneS2P()

for element in lista:
    new_name = "\\".join(["new", element])
    comment = raw_input("Scrivi il commento per %s: " % element)
    new.name = new_name
    new.comment = str(datetime.now()) + " " + comment
    new.invertiColonne(element)