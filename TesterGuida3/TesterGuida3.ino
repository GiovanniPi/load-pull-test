/*
Programma per la gestione della guida mobile che esegue 35 spostamenti (ogni spostamento ruota di 10° sulal carta di smith).
Presa come riferimento la frequenza 698MHz
Alcuni dati importanti:
num. giri medi: 69881 +- 3
mm per giro: 0.002034 mediata
escursione massima: 142.38 +- 0.01
scarto inziale: 6.23
massima escursione a 698MHz: 107.45mm
singolo scostamento(calcolati su 36 scarti): 2.98mm
*/

int p_dir = 4; //pin 2 usao controllo direzione motore: LOW->indietro, HIGH->avanti
int p_vel = 3; //setta velocità rotazione motore con PWM, 0 fermo, 255 max velocità
int fine_motore = 12; //pin fine corsa lato motore
int fine_aperta = 8; //pin fine corsa lato aperto
volatile long int encoder = 0; //numero di giri
char input;
boolean moving = false; //booleana che indica se la guida si sta muovendo o meno, va a false quando raggiunge un finecorsa o viene stoppata
char pin_fine_corsa;
long int target_pos;
long int a[35] = {1484,2945,4387,5853,7317,8761,10245,11713,13178,14641,16109,17569,19033,20503,21966,23433,24896,26362,27806,29294,30756,32220,33685,35154,36615,38084,39554,41021,42486,43950,45416,46880,48350,49814,51285};    //vettore che contiene le posizione hardcoded
int counter = 0;   //variabile counter

void setup(){  //funzione di inizializzazione
  Serial.begin(9600);
  pinMode(p_vel, OUTPUT);
  pinMode(p_dir, OUTPUT);
  pinMode(fine_motore, INPUT_PULLUP);  //controllo pin fine guida lato motore
  pinMode(fine_aperta, INPUT_PULLUP);  //controllo fine guida lato aperto
  analogWrite(p_vel, LOW);
   
  Serial.println("Lista comandi: ");
  Serial.println("p = prossima posizione");  Serial.println("r = azzera la guida");
  Serial.println("e = stampa encoder");
}

void loop(){ //ciclo principale in loop
  if(Serial.available()>0){
    input = Serial.read();
    Serial.write(input);
    switch(input){
      case 'p': Serial.print("Prossima posizione e': ");
                if(counter < 35){
                  target_pos = a[counter] - 34;
                  counter++;
                  Serial.println(target_pos);
                  attachInterrupt(0, posiziona_avanti, RISING);
                  avanzamento();
                }
                else{
                  Serial.println("Posizioni finite");
                }
                break;
      case 'r': resetta();
                break;
      case 'e': Serial.println(encoder);
                break;
    }
  }
  //controllo se fine guida (finita indica se in movimento)
  if(moving){     //<-------------------------1
    if(!fine_guida(pin_fine_corsa)){
      moving = false;
      Serial.println(encoder);
      detachInterrupt(0); //toglie l'interrupt
    }
  }
}

void avanzamento(){
  if(!fine_guida(fine_aperta))
    return;
  digitalWrite(p_dir, HIGH);
  analogWrite(p_vel, 255); 
  pin_fine_corsa = fine_aperta;
  moving = true;
}

void resetta(){
  if(!fine_guida(fine_motore))
    Serial.println("In posizione");
  detachInterrupt(0);
  digitalWrite(p_dir, LOW);
  analogWrite(p_vel, 255);
  do{
    if(Serial.available()>0)     //serva a svuotare la serail, in modo che i caratteri passati durante l'azzeramento vengano tutti eliminati
      input = Serial.read();
  }while(fine_guida(fine_motore));
  encoder = 0;
  Serial.println("Azzerato");
}    

//funzione controllo fine guida
int fine_guida(int pin){
  if(digitalRead(pin)==HIGH){ //controlla se pin high-->guida fine corsa 
      analogWrite(p_vel, 0);
      return 0;
  }
  return 1;
}

//funzione interrupt per encoder direzione avanti
void posiziona_avanti(){
  encoder++;
  if (encoder >= target_pos){
    analogWrite(p_vel, 0);
    moving = false;
  }
}
