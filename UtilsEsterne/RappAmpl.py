# -*- coding: utf-8 -*-
"""
Created on Tue Feb 17 13:51:53 2015

@author: emclab1
"""

import os
import sys
from PyQt4 import QtGui, QtCore
from BasicWidget import ButtonDD
from itertools import izip

class RapportoAmpl(QtGui.QDialog):
    banda_frequenza = {"GSM 850": '836000000.csv', "GSM 900": '896000000.csv',
                   "GSM 1800": '1747800000.csv', "GSM 1900": '1880000000.csv',
                   "UMTS 1": '1950000000.csv', "UMTS 2": '1880000000.csv',
                   "UMTS 4":'1732400000.csv', "UMTS 5":'836400000.csv',
                   "UMTS 8":'897400000.csv'}   
    
    def __init__(self, parent = None):
        super(RapportoAmpl, self).__init__(parent)
        self.idle = 0.0
        btn_url = ButtonDD('Carica una banda', self)
        btn_url.file_r.connect(self.urlPronto)
        self.edit_idle = QtGui.QLineEdit()
        reg_ex_curr = QtCore.QRegExp('^[0-9]*\.?[0-9]{1,4}$')
        self.edit_idle.setValidator(QtGui.QRegExpValidator(reg_ex_curr))
        self.edit_idle.editingFinished.connect(self.salvaIdle)
        self.edit_idle.cursorPositionChanged.connect(self.controllaZero)
        self.btn_calc = QtGui.QPushButton('Calcola')
        self.btn_calc.clicked.connect(self.calcola)
        self.btn_calc.setEnabled(False)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(btn_url)
        layout.addWidget(self.edit_idle)
        layout.addWidget(self.btn_calc)
        self.setLayout(layout)
        self.molt = 1

    def urlPronto(self, url):
        try:
            banda = str(url).split('/')[-1]
            if not self.banda_frequenza.has_key(banda):
                raise Exception('La cartella selezionata non è una banda')
            if 'GSM' in banda:
                self.molt = 8
                print self.molt
            else:
                self.molt = 1
            lista = os.listdir(url)
            if 'Consumi' in lista and 'Potenza' in lista:
                self.url = str(url)
                os.chdir(self.url)
                self.btn_calc.setEnabled(True)
                self.nome = self.banda_frequenza.get(banda)
            else:
                self.btn_calc.setEnabled(False)
        except Exception as e:
            QtGui.QMessageBox.warning(self, 'Errore', str(e))
            
    def calcola(self):
        try:
            for i in ['1','1.5','10','2','3','4','6','Open']:
            
                if not os.path.exists('Efficenza\\%s' % i):
                    os.makedirs('Efficenza\\%s'%i)
                name_pt = '\\'.join(['Potenza', i, self.nome])
                name_co = '\\'.join(['Consumi', i, self.nome])
                name_ga = '\\'.join(['Efficenza', i, self.nome])
                with open(name_pt, 'r') as p, open(name_co, 'r') as c, \
                     open(name_ga, 'w') as g:
                    for pot, con in izip(p, c):
                        line_pot = pot.strip().split(',')
                        line_con = con.strip().split(',')
                        gain = self.efficenza(float(line_pot[1]), 
                                             float(line_con[1]),
                                             float(line_con[2]))
                        new_line = ','.join([line_pot[0], gain,'\n'])
                        g.write(new_line)
        except Exception as e:
            QtGui.QMessageBox.warning(self, 'Errore', str(e))
            
    def efficenza(self, pot, volt, amp):
        return  str((10**(pot/10))/(volt * (amp - self.idle) * self.molt * 10))
    
    def controllaZero(self, prima, dopo):
        if dopo == 0:
            self.idle = 0.0
    
    def salvaIdle(self):
        self.idle = float(self.edit_idle.text())

app = QtGui.QApplication(sys.argv)
form = RapportoAmpl()
form.show()
app.exec_()