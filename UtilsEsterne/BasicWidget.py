# -*- coding: utf-8 -*-
"""
Created on Thu Aug 07 11:02:59 2014

@author: GiovanniPi
"""
from PyQt4 import QtGui, QtCore

class ButtonDD(QtGui.QPushButton):
    
    file_r = QtCore.pyqtSignal(object)
  
    def __init__(self, title, parent):
        super(ButtonDD, self).__init__(title, parent)
        
        self.setAcceptDrops(True)

    def dragEnterEvent(self, e):
      
        if e.mimeData().hasUrls():
            e.accept()
        else:
            e.ignore() 

    def dropEvent(self, e):
       url = e.mimeData().urls()
       aux = url[0].toLocalFile()
       self.file_r.emit(aux)