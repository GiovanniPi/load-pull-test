# -*- coding: utf-8 -*-
"""
Created on Fri Jan 30 09:51:41 2015

@author: GiovanniPi
"""

from PyQt4.QtGui import QPushButton
from PyQt4.QtCore import pyqtSignal


class BottoneDD(QPushButton):
    
    file_r = pyqtSignal(object)
  
    def __init__(self, title, parent):
        """Nome bottone e parent. Segnale drop valido è file_r(path)"""
        super(BottoneDD, self).__init__(title, parent)
        self.setMinimumSize(100, 40)
        
        self.setAcceptDrops(True)

    def dragEnterEvent(self, e):
      
        if e.mimeData().hasUrls():
            e.accept()
            
        else:
            e.ignore() 

    def dropEvent(self, e):
        
       url = e.mimeData().urls()
       aux = url[0].toLocalFile()
       self.file_r.emit(aux)