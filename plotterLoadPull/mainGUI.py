# -*- coding: utf-8 -*-
"""
Created on Fri Jan 30 09:28:20 2015

@author: GiovanniPi
"""

from PyQt4 import QtGui, QtCore
from BasicWidget import BottoneDD
from smith import draw
from itertools import islice

class ErroriPlot(Exception):
    
    def __init__(self, value):
        self._value = value
        
    def __str__(self):
        return repr(self._value)


class LoadPullPlotter(QtGui.QMainWindow):
    
    bande_valide = ('GSM 850','GSM 900','GSM 1800','GSM 1900',
                    'UMTS 1','UMTS 2','UMTS 4','UMTS 5', 'UMTS 8')
    misure_valide = ('Potenza', 'Armoniche', 'Radiazione', 'Sensibilita', 
                     'Potenza FSV', 'Consumi', 'Efficenza')
    swr_validi = ('1','1.5','2','3','4','6','10','Open')
    #regola: nome misura, livello max, delta max-min, intervalli per linee, 
    #numero di intervalli per contorni, indice delta
    misure_params = {'Armoniche':['Harmonics', -30, -20, 15, 50, 0],
                     'Consumi':['Consumption', 2.5, -0.5, 20, 30, 1],
                     'Potenza':['Power', 36, -6, 12, 40, 2],
                     'Sensibilita':['Sensitivity', -107, -7, 14, 40, 3],
                     'Radiazione':['Radiation',-30, -20, 15, 50, 4],
                     'Potenza FSV':['Power', 36, -6, 10, 40, 5],
                     'Efficenza':['Efficency', 50, -45, 15, 50, 6]}
    #variazione delle singole bande rispetto ai parametri default salvati
    #in misure_params. I delta sono:
    #delta pot, delta cons, delta power, delta sens, delta rad, delta pot fsv
    delta_bande = {'GSM 1900':[0, -0.35, -3, 0, 0, -3, 0], 
                   'GSM 1800':[0, -0.35, -3, 0, 0, -3, 0],
                   'UMTS 1':[0, -1.5, -8, 0, 0, -8, 0],
                   'UMTS 2':[0, -1.5, -8, 0, 0, -8, 0],
                   'UMTS 4':[0, -1.5, -8, 0, 0, -8, 0],
                   'UMTS 5':[0, -1.5, -8, 0, 0, -8, 0],
                   'UMTS 8':[0, -1.5, -8, 0, 0, -8, 0]}
    #associa la banda ai canali (aggiungere limiti per le misure)
    canali = {'GSM 1900':'TCH 661', 'GSM 1800':'TCH 700', 'GSM 900':'TCH 30', 
              'GSM 850':'TCH 187', 'UMTS 1':'UARFCN 9750', 'UMTS 2':'UARFCN 9400', 
              'UMTS 4':'UARFCN 1412', 'UMTS 5':'UARFCN 4282', 
              'UMTS 8':'URAFCN 2787'}
    
    def __init__(self):
        """Parametri misura salva il vettore dei parametri della misura 
        attualmente selezionata\n
        Aggiusta bande tiene traccia dei modificatori di msiura relativi alle 
        singole bande\n
        """
        super(LoadPullPlotter, self).__init__()
        self.setWindowTitle('Load Pull Plotter')
        self.nome_sess = QtGui.QLabel('')
        self.file_tree = [None, None, None]
        self.file_list = []
        self.parametri_misura = None
        self.aggiusta_bande = None
        
        self.statusBar()        
        
        self.lista_swr = QtGui.QListWidget()
        self.lista_swr.setFixedWidth(90)
        self.lista_swr.itemClicked.connect(self.swrSelezionato)
        self.lista_swr.setStatusTip('Seleziona un swr per vedere le frequenze')
        
        self.combo_banda = self.creaCombo(self.bandaSelezionata, 
                           'Seleziona una banda per vedere le misure disponibili')
        self.combo_misura = self.creaCombo(self.misuraSelezionata,
                           'Seleziona una misura per vedere i carichi associati')
        self.combo_freq = self.creaCombo(self.freqSelezionata,
                           'Seleziona una frequenza')

        btn_dir = BottoneDD('Session folder', self)
        btn_dir.file_r.connect(self.dirPronta)
        self.btn_draw = QtGui.QPushButton('Draw')
        self.btn_draw.clicked.connect(self.plot)
        self.btn_draw.setEnabled(False)
        self.btn_draw.setMinimumSize(100, 40)
        self.btn_save_data = QtGui.QPushButton('Save data')
        self.btn_save_data.clicked.connect(self.salvaDati)
        self.btn_save_data.setEnabled(False)
        
        gruppo_folder = self.creaGruppo('Select data to plot', QtGui.QVBoxLayout(),
                                        'Select band', self.combo_banda, 
                                        'Select test type', self.combo_misura, 
                                        'Select frequency', self.combo_freq)
        gruppo_swr = self.creaGruppo('SWR', QtGui.QVBoxLayout(), self.lista_swr)
        
        reg_ex = QtCore.QRegExp('-?\\d{1,15}')
        reg_ex_curr = QtCore.QRegExp('^[0-9]*\.?[0-9]{1,4}$')
        
        self.edit_imei = self.creaEdit(self.formattaMod, reg_ex,
                        'IMEI modulo usato')
        self.edit_idle = self.creaEdit(self.formattaMod, reg_ex_curr, 
                        'Corrente assorbita in ampere dal modulo in idle (sono validi '
                        'numeri decimali separti da virgola, non negativi)')
        self.edit_sw = self.creaEdit(self.formattaMod, 
                        tip = 'Versione software')
        self.edit_cs = self.creaEdit(self.formattaMod, 
                        tip = 'Codice CS modulo (alternativa nome transducer)')
               
        gruppo_data_mod = self.creaGruppo('Module data', QtGui.QVBoxLayout(),
                                          'IMEI', self.edit_imei, 
                                          'Idle current', self.edit_idle,
                                          'SW Version', self.edit_sw,
                                          'CS Number', self.edit_cs)  
        
        self.edit_nome = self.creaEdit(self.formattaMis, 
                         tip = 'Tipo di misura da plottare')
        self.edit_tch = self.creaEdit(self.formattaMis, 
                        tip = 'Inserisci il canale di misura (viene impostato '
                        'il canale centrale in base alla banda selezionata)')
        self.edit_ref_plane = self.creaEdit(self.formattaMis, 
                        tip = 'Piano di riferimento per la misura')
        self.edit_note = self.creaEdit(self.notePronte, 
                        tip = 'Note aggiuntiva della misura')
                
        gruppo_data_mis = self.creaGruppo('Test data', QtGui.QVBoxLayout(),
                                          'Type', self.edit_nome,
                                          'Channel', self.edit_tch,
                                          'Reference plane', self.edit_ref_plane,
                                          'Addictional note', self.edit_note)
        
        self.label_mod = QtGui.QLabel('')
        self.label_mis = QtGui.QLabel('')
        self.label_not = QtGui.QLabel('')
        
        gruppo_preview = self.creaGruppo('Preview', QtGui.QVBoxLayout(),
                                         self.label_mod, self.label_mis,
                                         self.label_not)
        
        grid = QtGui.QGridLayout()
        grid.addWidget(btn_dir, 0, 0, 1, 1)
        grid.addWidget(self.nome_sess, 0, 1, 1, 4)
        grid.addWidget(gruppo_folder, 1, 0, 1, 1)
        grid.addWidget(gruppo_swr, 1, 1, 1, 1)
        grid.addWidget(gruppo_data_mod, 1, 2, 1, 1)
        grid.addWidget(gruppo_data_mis, 1, 3, 1, 1)
        grid.addWidget(gruppo_preview, 2, 0, 2, 3)
        grid.addWidget(self.btn_draw, 2, 3, 1, 1)
        grid.addWidget(self.btn_save_data, 3, 3, 1, 1)
        
        main_wid = QtGui.QWidget()
        main_wid.setLayout(grid)
        #assoccia il campo di salvataggio nel file DatiMisura alla edit corretta
        self.load_dati = {'IMEI': self.edit_imei, 'CORRENTE IDLE':self.edit_idle, 
                     'CS INTERFACE':self.edit_cs, 'SW':self.edit_sw,
                     'CALIBRATION PLANE':self.edit_ref_plane, 
                     'NOTE':self.edit_note}
        
        self.setCentralWidget(main_wid)

    def dirPronta(self, path):
        self.pulisciCampi(self.combo_banda, self.combo_freq, self.combo_misura,
                          self.lista_swr, self.btn_draw, self.edit_cs,
                          self.edit_idle, self.edit_imei, self.edit_note,
                          self.edit_nome, self.edit_ref_plane, self.edit_sw,
                          self.edit_tch)
        qdir = QtCore.QDir(path)
        lista = qdir.entryList(qdir.Dirs|qdir.NoDotAndDotDot)
        try:
            self.aggiungiItem(lista, self.combo_banda, self.bande_valide)
            qdir.setCurrent(path)
            self.file_tree[0] = str(path)
            self.nome_sess.setText(self.file_tree[0].split('/')[-1])
            self.formattaMod()
            self.loadData() #funzione per caricare dati di misura se presenti
            self.btn_save_data.setEnabled(True)
        except ErroriPlot:
            QtGui.QMessageBox.warning(self, 'Attenzione', 
                "La cartella selezionata non e' una sessione valida")
            
    def bandaSelezionata(self, banda):
        self.pulisciCampi(self.combo_freq, self.combo_misura, self.lista_swr,
                          self.btn_draw)
        qdir = QtCore.QDir(self.file_tree[0])
        qdir.setPath(banda)
        lista = qdir.entryList(qdir.Dirs|qdir.NoDotAndDotDot)
        try:
            self.aggiungiItem(lista, self.combo_misura, self.misure_valide)
            self.file_tree[1] = str(self.file_tree[0]) + '/' + str(banda)
            self.aggiusta_bande = self.delta_bande.get(str(banda), 
                                                       [0, 0, 0, 0, 0, 0, 0])
            self.edit_tch.setText(self.canali.get(str(banda)))
            self.formattaMis()
        except ErroriPlot:
            QtGui.QMessageBox.warning(self, 'Attenzione', 'Banda vuota')
            
    def misuraSelezionata(self, misura):
        self.pulisciCampi(self.combo_freq, self.lista_swr, self.btn_draw)
        qdir = QtCore.QDir('/'.join([self.file_tree[1], str(misura)]))
        lista = qdir.entryList(qdir.Dirs|qdir.NoDotAndDotDot)
        try:
            self.aggiungiItem(lista, self.lista_swr, self.swr_validi)
            self.file_tree[2] = self.file_tree[1] + '/' + str(misura)
            self.parametri_misura = self.misure_params.get(str(misura))
            self.edit_nome.setText(self.parametri_misura[0])
            self.formattaMis()
        except ErroriPlot:
            QtGui.QMessageBox.warning(self, 'Attenzione', 'Misura vuota')
            
    def swrSelezionato(self, item):
        self.pulisciCampi(self.combo_freq, self.btn_draw)
        qdir = QtCore.QDir('/'.join([self.file_tree[2], str(item.text())]))
        qdir.setNameFilters(['*.csv','*.CSV'])
        lista = qdir.entryList(qdir.Files)
        if lista:
            for item in lista:
                self.combo_freq.addItem(item.split('.')[0])
        else:
            QtGui.QMessageBox.warning(self, 'Attenzione', 
            'Nessuna frequenza trovata')
            
    def freqSelezionata(self, item):
        del self.file_list[:]
        qdir = QtCore.QDir(self.file_tree[2])
        if qdir.exists():
            lista = qdir.entryList(QtCore.QDir.Dirs | 
                                      QtCore.QDir.NoDotAndDotDot)
        
        for cart in lista:
            var = "\\".join([str(self.file_tree[2]), str(cart), 
                             str(item) + ".csv"])        
            if qdir.exists(var):
                self.file_list.append(var)
        if self.file_list:
            self.btn_draw.setEnabled(True)
        else:
            self.btn_draw.setEnabled(False)
        if self.parametri_misura[0] == 'Harmonics':
            indice = self.combo_freq.findText(item, QtCore.Qt.MatchExactly)
            if indice == 0:
                self.edit_nome.setText('2nd Harmonic')
            elif indice == 1:
                self.edit_nome.setText('3nd Harmonic')
            else:
                self.edit_nome.setText('4th Harmonic')
            self.formattaMis()
            
    def pulisciCampi(self, *args):
        for item in args:
            if isinstance(item, QtGui.QPushButton):
                item.setEnabled(False)
                continue
            if isinstance(item, QtGui.QLineEdit):
                item.setText('')
                continue
            item.clear()
        
    def loadData(self):
        """Carica i dati di misura se e' presente nelal cartella di sessione
        il file DatiMisura.txt"""
        v = []
        try:
            with open('DatiMisura.txt', 'r') as f:
                v= list(islice(f, 0 , None))
            if v:
                self.caricaForm(v)
        except IOError:
            pass
        
    def salvaDati(self):
        address = self.file_tree[0] + '/' + 'DatiMisura.txt'
        with open(address, 'w') as f:
            if self.edit_imei.text():
                f.write('IMEI:' + str(self.edit_imei.text()) + '\n')
            if self.edit_idle.text():
                f.write('CORRENTE IDLE:' + str(self.edit_idle.text()) + '\n')
            if self.edit_sw.text():
                f.write('SW:' + str(self.edit_sw.text()) + '\n')
            if self.edit_cs.text():
                f.write('CS INTERFACE:' + str(self.edit_cs.text()) + '\n')
            if self.edit_ref_plane.text():
                f.write('CALIBRATION PLANE:' + str(self.edit_ref_plane.text()) + '\n')
            if self.edit_note.text():
                f.write('NOTE:' + str(self.edit_note.text()) + '\n')               

    def caricaForm(self, lista):
        for item in lista:
            spezza = item.split(':')
            label = self.load_dati.get(spezza[0], None)
            if label:
                label.setText(spezza[1].strip())
        self.formattaMis()
        self.formattaMod()
        self.notePronte()
        
    def formattaMod(self):
        self.label_mod.setText('%s, IMEI: %s, SW: %s, %s' % \
                                                (str(self.nome_sess.text()),
                                                 str(self.edit_imei.text()),
                                                 str(self.edit_sw.text()),
                                                 str(self.edit_cs.text())
                              ))
                                                 
    def formattaMis(self):
        self.label_mis.setText('%s, %s, %s, Ref. plane: %s' % \
                        (str(self.combo_banda.currentText()),
                         str(self.edit_nome.text()),
                         str(self.edit_tch.text()),
                         str(self.edit_ref_plane.text())
                         ))
                         
    def notePronte(self):
        self.label_not.setText(self.edit_note.text())
                                             
    def plot(self):
        vect = self.parametri_misura[:]
        ind = self.parametri_misura[-1] 
        vect[1] = self.parametri_misura[1] + self.aggiusta_bande[ind]
        if vect[0] == 'Consumption':
            delta = float(self.edit_idle.text())
            if 'GSM' in str(self.combo_banda.currentText()):
                molti = 8
            else:
                molti = 1
        else:
            delta = 0
            molti = 1
        print delta
        draw(self.file_list, delta, molti, vect, str(self.label_mod.text()), 
             str(self.label_mis.text()), str(self.label_not.text()))
        
    def creaGruppo(self, nome, layout, *args):
        gruppo = QtGui.QGroupBox(nome)
        for item in args:
            if type(item) is str:
                layout.addWidget(QtGui.QLabel(item))
                continue
            layout.addWidget(item)
        layout.addStretch()
        gruppo.setLayout(layout)
        return gruppo
        
    def aggiungiItem(self, lista, combo, validi):
        """lista e' il risultato di entryList, combo puo' essere una combo
        box oppure una listWidget, validi e' il vettore che contiene le 
        stringhe valide"""
        for index, item in enumerate(lista):
            if str(item) not in validi:
                lista.removeAt(index)
        if len(lista) == 0:
            raise ErroriPlot(' ')
        combo.addItems(lista)
            
    def creaEdit(self, slot, reg = None, tip = None):
        """Tip e' il suggerimento, reg e' la regular expression associata.
        Deve essere di tipo QtCore.QRegExp"""
        edit = QtGui.QLineEdit()
        edit.editingFinished.connect(slot)
        if tip:
            edit.setStatusTip(tip)
        if reg:
            edit.setValidator(QtGui.QRegExpValidator(reg))
        return edit

    def creaCombo(self, slot, tip = None):
        combo = QtGui.QComboBox()
        combo.activated[QtCore.QString].connect(slot)
        if tip:        
            combo.setStatusTip(tip)
        return combo

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    form = LoadPullPlotter()
    form.show()
    sys.exit(app.exec_())