# -*- coding: utf-8 -*-
"""
Created on Thu Nov 27 10:31:13 2014

@author: GiovanniPi
"""

"""Prove per disegnare una carta di smith più efficente e che sia utilizzabile 
come metodo di una classe"""


from matplotlib.patches import Circle
import matplotlib.pyplot  as plt
from matplotlib.mlab import griddata
import numpy as np

def draw(file_list, delta, molti, param, title1 = None, title2 = None, 
         note = None):

    plt.ion()

    c_x, c_y, c_z = [], [], []
    for name in file_list:
        with open(name, "r") as f:        
            for line in f: 
                line_split = line.split(",")
                xy = np.complex(line_split[0])    
                c_x.append(np.real(xy))
                c_y.append(np.imag(xy))
                if param[0] == 'Consumption':
                    val = (float(line_split[2]) - delta) * molti
                else:
                    val = float(line_split[1]) - delta
                c_z.append(val)

    fig = plt.figure("smith", figsize = (11.25, 13.5))
      
    asse = fig.add_axes([0.03, 0.03, 0.95, 0.95])
    asse.set_xlim(-1.1, 1.1)
    asse.set_ylim(-1.1, 1.2)


    asse.text(0.01, 0.0, '50', fontsize=7, color = 'k')
    asse.text(-0.33, 0.0, '25', fontsize=7, color = 'k')
    asse.text(-0.66, 0.0, '10', fontsize=7, color = 'k')
    asse.text(0.345, 0.0, '100', fontsize=7, color = 'k')
    asse.text(0.675, 0.0, '250', fontsize=7, color = 'k')
    
    #testo per cerchi a ammettenza  costante
    #positivi
    asse.text(0.02, 0.96, '+50j', fontsize=7, color = 'k')
    asse.text(-0.58, 0.79, '+25j', fontsize=7, color = 'k')
    asse.text(-0.9, 0.39, '+10j', fontsize=7, color = 'k')
    asse.text(0.57, 0.73, '+100j', fontsize=7, color = 'k')
    
    #negativi
    asse.text(0.02, -0.975, '-50j', fontsize=7, color = 'k')
    asse.text(-0.57, -0.81, '-25j', fontsize=7, color = 'k')
    asse.text(-0.9, -0.41, '-10j', fontsize=7, color = 'k')
    asse.text(0.585, -0.75, '-100j', fontsize=7, color = 'k')
        
    #testo VSWR
    asse.text(0.095, 0.095, 'VSWR=1.5\nRL=13.98', fontsize=7, color = 'b')
    asse.text(0.19, 0.19, 'VSWR=2\nRL=9.54', fontsize=7, color = 'b')
    asse.text(0.305, 0.305, 'VSWR=3\nRL=6.02', fontsize=7, color = 'b')
    
    asse.text(0.426, 0.426, 'VSWR=5\nRL=3.52', fontsize=7, color = 'b')
    asse.text(0.535, 0.535, 'VSWR=10\nRL=1.74', fontsize=7, color = 'b')

    clip1 = Circle((0,0), 1, ec = 'black', fc = 'none', lw = 1)
    asse.add_patch(clip1)

    for r in [0.2, 0.5, 1.0, 2.0, 5.0]:
        centro = (r/(1.+r),0)
        raggio = 1./(1+r)
        line = Circle(centro, raggio, ec = 'black', fc = 'none', lw = 0.2)
        asse.add_patch(line)
        
    for r in [0.2, 0.333, 0.5, 0.666, 0.818]:
        line = Circle((0, 0), r, ec = 'b', fc = 'none', lw = 0.2)
        asse.add_patch(line)

    griglia = []
    for x in [0.5, 1, 2, 5]: #0.2, 
        line = Circle((1,x), x, ec = 'black', fc = 'none', lw = 0.2)
        line1 = Circle((1,-x), x, ec = 'black', fc = 'none', lw = 0.2)
        griglia.append(line)
        griglia.append(line1)

    for item in griglia:
        cc = asse.add_patch(item)
        cc.set_clip_path(clip1)

    asse.yaxis.set_ticks([])
    asse.xaxis.set_ticks([])

    xi = np.linspace(-1, 1, 500)
    yi = np.linspace(-1, 1, 500)
    zi = griddata(c_x, c_y, c_z, xi, yi, interp='nn')
    if param[0] == 'Power' or param[0] == 'Efficency':
        mapp =  plt.cm.rainbow_r
    else:
        mapp = plt.cm.rainbow
    CS2 = asse.contour(xi, yi, zi, param[3], linewidths=0.5, colors='k',linestyles ='--')
    if param[0] == 'Consumption':
        plt.clabel(CS2, inline = 1, fmt = "$%2.2f$"+"$\ A$", fontsize = 12, style = 'italic', colors='k') 
    elif param[0] == 'Efficency':
        plt.clabel(CS2, inline = 1, fmt = "$%2.1f$"+"$\ perc$", fontsize = 12, style = 'italic', colors='k')
    else:
        plt.clabel(CS2, inline = 1, fmt = "$%2.1f$"+"$\ dBm$", fontsize = 12, style = 'italic', colors='k')
    asse.contourf(xi, yi, zi, param[4], cmap=mapp, vmax=param[1], 
                       vmin=param[1] + param[2])
    if title1:
        asse.text(0, 1.15, title1, fontsize = 18, ha = 'center')
    if title2:
        asse.text(0, 1.08, title2, fontsize = 18, ha = 'center')
    if note:
        asse.text(0, -1.07, note, fontsize = 18, ha = 'center')
    
    plt.draw()