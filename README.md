# Multipurpose Input Impedance Tuner Test #

The program LoadPull allow to test power, sensitivity, harmonics, radiation and power consumption of the EUT. The test is performed by changing the length of a line stretcher and the load shown at the input of the EUT

## Test Equipment ##

  * Spectrum analyzer mod.FSV13
  * CMU200 ser. 837983/014(GSM only) or CMU200 ser. (GSM+UMTS)
  * DC PowerSupply HMP2020 
  * DC PowerSupply E3615A
  * Line stretcher
  * Spectrum analyzer mod.HP8563E

## Dependancy ##

* Python 2.7.6
* Qt 4.8.4; PyQt 4.9.6; SIP 4.14.2
* pyVisa 1.6.4
* pyqtGraph 0.9.10