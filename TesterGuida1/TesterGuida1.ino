/*
Programma per la gestione della guida mobile. In questa versione ci interessa solo mandare la guida avanti e indietro per capire il numero di giri 
che esegue e l'escursione. nel file Test1.xls vengono salvati i dati inizio, fine, escursione, num_giri
*/

int p_dir = 4; //pin 2 usao controllo direzione motore: LOW->indietro, HIGH->avanti
int p_vel = 3; //setta velocità rotazione motore con PWM, 0 fermo, 255 max velocità
int pin_encoder = 0; //numero interrupt, corrisponde al pin 2
int fine_motore = 12; //pin fine corsa lato motore
int fine_aperta = 8; //pin fine corsa lato aperto
volatile long int encoder = 0; //numero di giri
char input;
boolean finita = false; //booleana che indica se la guida è a fine corsa o meno, serve nei putni 1 e 2 (ha logica negata->true=NO, false=Si)
char pin_fine_corsa;


void setup() {  //funzione di inizializzazione
  Serial.begin(9600);
  pinMode(p_vel, OUTPUT);
  pinMode(p_dir, OUTPUT);
  pinMode(fine_motore, INPUT_PULLUP);  //controllo pin fine guida lato motore
  pinMode(fine_aperta, INPUT_PULLUP);  //controllo fine guida lato aperto
  analogWrite(p_vel, LOW);
  
  /* Per comunicazione seriale tramite Python non serve il menu
  Serial.println("Lista comandi: "); 
  Serial.println("a = avanti");
  Serial.println("b = indietro");
  Serial.println("s = stop");
  */
}

void loop(){ //ciclo principale in loop
  if(Serial.available()>0){
    input = Serial.read();
    switch(input){
      case 'a': attachInterrupt(pin_encoder, conta_giri_avanti, RISING);
                avanzamento('a');
                Serial.write('a');
                break;
      case 'b': attachInterrupt(pin_encoder, conta_giri_indietro, RISING);
                indietreggia('b');
                Serial.write('b');
                break;
      case 's': analogWrite(p_vel, 0);
                Serial.write('s');
                break;
      default: Serial.write('e');
                break;
    }
   
  }
  //controllo se fine guida (finita indica se in movimento)
  if(finita){     //<-------------------------1
    if(!fine_guida(pin_fine_corsa)){
      finita = false;
      Serial.print(encoder);
      encoder = 0;
      detachInterrupt(0); //toglie l'interrupt
    }
  }
}

void avanzamento(char valore) {
  if(!fine_guida(fine_aperta))
    return;
  digitalWrite(p_dir, HIGH);
  analogWrite(p_vel, 255); 
  pin_fine_corsa = fine_aperta;
  finita = true;
}

void indietreggia(char valore){
  if(!fine_guida(fine_motore))
    return;
  digitalWrite(p_dir, LOW);
  analogWrite(p_vel, 255); 
  pin_fine_corsa = fine_motore;
  finita = true;
}    

void ferma(){
  analogWrite(p_vel, 0);
}

//funzione controllo fine guida
int fine_guida(int pin){
  if(digitalRead(pin)==HIGH){ //controlla se pin high-->guida fine corsa 
      analogWrite(p_vel, 0);
      return 0;
  }
  return 1;
}

//funzione interrupt per encoder direzione avanti
void conta_giri_avanti(){
  encoder++;
}

//funzione interrupt per encoder direzione indietro
void conta_giri_indietro(){
  encoder--;
}
