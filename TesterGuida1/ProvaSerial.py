# -*- coding: utf-8 -*-
"""
Created on Tue Oct 28 09:41:54 2014

@author: GiovanniPi
"""

PORT = "COM12"
SPEED = 9600

import serial
from threading import Thread

message = {'a':"Avanti", 'b':'Indietro', 's':'stop', 'e':'Error',
           'f':"fine guida"}

def receiver(ser):
    global message
    while True:
        if ser.inWaiting()>0:
            answer = ser.read
            print "Thread: "
            print answer
            

class ToArduino(object):
      
    def __init__(self):
        self.connection = serial.Serial(PORT, SPEED)
        
        self.thread = Thread(target = receiver, args=(self.connection,)).start()
        
    @property
    def l(self):
        self.connection.write('a')
        
    @property
    def r(self):
        self.connection.write('b')
        
    @property
    def s(self):
        self.connection.write('s')
        
    @property
    def c(self):
    
        self.connection.close()
#a = ToArduino()